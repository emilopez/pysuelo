# Proyecto de medición de humedad de suelo

Emiliano López - emiliano.lopez@gmail.com \
Doctorando en Ingeniería, mención recursos hídricos.\
Magister en Computación Aplicada a las Ciencias y la Ingeniería.\
Ingeniero en Informática

Centro de Estudios Hidro-Ambientales, Facultad de Ingeniería y Ciencias Hídricas, Universidad Nacional del Litoral. Santa Fe. Argentina

## Introducción
En el presente trabajo se documentan una serie de mediciones de humedad de suelo utilizando diferentes tipo de sensores. El registro de la humedad del suelo se realiza en combinación con otras mediciones de variables íntimamente relacionadas como la temperatura del suelo, la humedad y temperatura ambiente. Además se realizará un análisis granulométrico para clasificar la composición del suelo donde se instalen los sensores.

Para llevar a cabo estas pruebas se diseñaron experimentos en una primera instancia en laboratorio y luego en campo. A continuación se describen los componentes a utilizar y el diseño de pruebas.


## Metodología

A continuación se detallan los elementos necesarios para llevar a cabo las mediciones.

### Componentes

#### Sensores

- Hydra-Probe II de Stevens (2 sensores): sensor de humedad de suelo se ha convertido en estándar de facto. Se utilizarán 2 para contrastar con el resto de los sensores.
- Capacitive soil moisture 1.2 (10 sensores): sensor económico que se pretende calibrar
- Sensirion SHT10 (2 sensores): sensor capacitivo de humedad y temperatura del suelo
- DS18B20 (3 sensores): temperatura de suelo
- DHT22 (4 sensores): temperatura y humedad ambiente

#### Balanza de precisión
Balanza Ohaus Scout Pro, rango de 0 a 6000 gramos, 0.1 gramos de resolución.

#### Datalogger

Las variables se registrarán con un dispositivo diseñado para tal fin que combina los siguientes elementos:

- Microcontrolador Arduino pro mini
- Módulo RTC DS3231
- Módulo almacenamiento catalex SD


## Diseño de pruebas

### Gravimetría

La gravimetría es la primera prueba ya que determina exactamente el contenido volumétrico de agua en una muestra de suelo a través del pesaje en una balanza de precisión.

El proceso consiste en los siguientes pasos:

- Secado total de la muestra de suelo en un horno
- Pesaje de la muestra de suelo en conjunto con el recipiente y los sensores
- Agregado de de un volumen conocido de agua
- Registro en simultáneo de los valores arrojado por los sensores y la balanza

Posteriormente se hará un análisis granulométrico para clasificar el tipo de suelo. Este experimento se replicará con diferentes tipos de suelo (diferente proporción de limos, arcillas y arenas).

Para automatizar el registro de la balanza se desarrolló un programa que toma fotos del visor de la balanza cada un período configurable. Posteriormente esto se contrastará con los valores de los sensores para obtener una calibración mediante una regresión.

### Pruebas en campo

Una vez superada la etapa de laboratorio se instalarán en campo bajo las siguientes configuraciones:

- A una misma profundidad, en un mismo punto (muy cercanos)
- En una misma vertical a diferentes profundidades
- A una misma profundidad en diferentes puntos del terreno

## Resultados esperables

Como resultado de estas pruebas se espera obtener un conjunto de mediciones de la humedad de suelo registrada por diferentes sensores y de otras variables que afectan indirectamente su dinámica.

Con este conjunto de datos se pretende luego utilizar técnicas de regresión mediante Machine Learning.

## Referencias

### SHT10
- https://www.adafruit.com/product/1298
- https://github.com/practicalarduino/SHT1x
- https://learn.sparkfun.com/tutorials/sht15-humidity-and-temperature-sensor-hookup-guide
