# Script python como servicio con systemd


## Script 
- Almacenado en: /home/pi/pyescucha.py

```python
#!/usr/bin/env python3

import serial
import datetime
from time import sleep
import sys

# buscamos el puerto serie/usb
baud = 9600
baseports = ['/dev/ttyUSB', '/dev/ttyACM', 'COM', '/dev/tty.usbmodem1234']
ser = None
while not ser:
    for baseport in baseports:
        if ser:
            break
        for i in range(0, 64):
            try:
                port = baseport + str(i)
                ser = serial.Serial(port, baud, timeout=1)
                print("Monitor: Opened " + port + '\r')
                break
            except:
                ser = None
                pass
# Por ronda leemos una vez cada uno de los sensores
cant_sensores = 15
# veces q repetimos la lectura
ronda_lectura = 10
# tiempo que esperamos para volver a medir
periodo_minutos = 15
while True:
    for ronda in range(ronda_lectura):
        sensores_leidos = 0
        lecturas = list()
        while sensores_leidos < cant_sensores:
            val = str(ser.readline().decode().strip('\r\n'))#Capture serial output as a decoded string
            if val:
                sensores_leidos += 1
                val = val.replace("-",";-").replace("+",";")
                lecturas.append(val)
        timestamp = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        fout = open("/home/pi/test_lectura_borrar.csv", "a")
        for lectura in lecturas:
            s = str(timestamp) + ";" + str(lectura)
            fout.write(s + "\n")
            print(s)
        print()
        fout.close()
    print("ZZzzZZzz...")
    sleep(periodo_minutos*60)

```

## Systemd config

- Creamos ``pyescucha.service`` en ``/etc/systemd/system``

```
[Unit]
Description=lee puerto serie de arduino

[Service]
Type=simple
ExecStart=/home/pi/pyescucha.py
StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=pyescucha

[Install]
WantedBy=multi-user.target
```

Luego:
- Para habilitarlo que se ejecute al inicio: ``systemctl enable pyescucha``
- Para deshabilitarlo que se ejecute al inicio: ``systemctl disable pyescucha``
- Para ejecutarlo: ``systemctl start pyescucha``
- Para saber el estado: ``systemctl status pyescucha``
- Para detenerlo: ``systemctl stop pyescucha``
- Si cambiamos algo en el script es mejor recargar el servicio: systemctl daemon-reload


