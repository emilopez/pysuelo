

# Instalación de equipos para cultivos

Se instalaron dos equipos en campo, uno para registrar el crecimiento de cultivos y otro el nivel freático, ambos registran además humedad y temperatura ambiente. Acceder a los puntos para [navegar mapa interactivo](http://umap.openstreetmap.fr/es/map/puntos-de-observacion-fichfca_523349)

## Información general

- Fecha: 12-11-2020
- Proyectos marco: CAID 2016, CAID 2020
- Destino: Campo ubicado entre Humboldt y Nuevo Torino
- Horario: 11hs - 16.30hs (salida y arribo a SFe) 
- Integrantes: Jorge Prodolliet, Guillermo Contini, German Dunger, José Maiztegui y Emiliano López
- Distancia: 150km recorridos
- Gastos: $ 1300.20 (combustible), $70 (peajes), $570 (almuerzo)
<centre>
<img src="img/primeros_dos_equipos.png" alt="eq4 crecimiento cultivos" title="alfalfa" width="550"/>

Ubicación de los equipos instalados 
</centre>



## Equipos instalados
- **Equipo 1**: Nivel freático, humedad y temperatura ambiente [(-31.34129, -61.13881)](https://maps.me/ge0?latlonzoom=8MxpotKqa8&name=Eq1-nivel_freatico). Instalado en molino caído, maíz sembrado.
  - Encendido: 12-11-2020 14.45hs (aprox)
  - Longitud de sonda: 7mts
  - Altura de brocal: 0.30 mts
  - Lectura inicial: 124 cmca -> NF = 7-0.3-1.24 = 5.46mts

- **Equipo 4**: Crecimiento de 5 parcelas de cultivo, humedad y temperatura ambiente [(-31.32234, -61.13441)](https://maps.me/ge0?latlonzoom=8MxpqHvkJc&name=Eq4-cultivos). Instalado al lado del potrero, alfalfa sembrada.
  - **Encendido: 16-11-2020 16hs (aprox)**

<img src="img/c4.jpeg" alt="eq4 crecimiento cultivos" title="alfalfa" width="300"/>
<img src="img/c2.jpeg" alt="eq4 crecimiento cultivos" title="alfalfa" width="300"/>
<br/>
<img src="img/nf1.jpg" alt="eq1 nivel freatico" title="nf" width="300"/>
<img src="img/nf2.jpg" alt="eq1 nivel freatico" title="nf" width="300"/>
<br/>

## Observaciones
- Se despegó el panel de la estación de cultivos, quedó sujetado con precinto. Llevar pegamento el próximo viaje.
- La estación de cultivos quedó inicialmente apagada, el día 16-11-2020 José se reubicó y encendió
- Posiblemente la semana del 23/11 iremos a instalar al resto de los equipos
- *[Versión online de este documento](https://gitlab.com/emilopez/pysuelo/-/blob/master/reportes/20201112.md)* 