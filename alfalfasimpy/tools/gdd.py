import numpy as np
import pandas as pd

def compute_gdd(df, tref=5): 
    """compute GDD using trapezoidal numerical integration method, 
       adding two columns for df: gdd_fino and gdd_acum
        df: input pandas dataframe must have columns names: 
            - datetime: datetime
            - timelinux: pd.to_datetime(df['datetime']).astype('int64')/10**18
            - t: temperature
        tref: reference temperature
    """
    integrales = []
    test_fechas = []
    df["datetime"] = pd.to_datetime(df["datetime"])
    for grupo in df.groupby(df.datetime.dt.date)[['datetime', 't']]:
        fechas = grupo[1]['datetime'].to_numpy()
        #x = grupo[1]['timelinux'].to_numpy()  # cada grupo es una tupla de dos elementos, 
        y = grupo[1]['t'].to_numpy()          # en la pos 1 está el dataframe de cada grupo
        # número de trapecios
        n = len(y) - 1
        # primer GDD 0
        integrales.append(0)
        test_fechas.append(0)
        for i in range(n):
            delta_fechas  = fechas[i+1] - fechas[i]
            delta_minutos = (delta_fechas/ np.timedelta64(1, 's'))/ 60
            delta_hora    = delta_minutos/60
            delta_dia     = delta_hora/24
            test_fechas.append(delta_dia)
            tavg = ((y[i+1] + y[i]) / 2 ) - tref
            if tavg < 0:
                tavg = 0
            area_i = (delta_dia) * tavg
            integrales.append(area_i)
            
    df["gdd_fino"]  = np.array(integrales)
    df["gdd_acum"]  = df["gdd_fino"].cumsum()
    df["delta_dia"] = np.array(test_fechas)
    df["delta_dia_acum"] = df["delta_dia"].cumsum()
    return df

def compute_gdd_v3(df, tref=5): # version mia arreglada, deberia reemplazar a compute_gdd 
    """compute GDD using trapezoidal numerical integration method, 
       adding two columns for df: gdd_fino and gdd_acum
        df: input pandas dataframe must have columns names: 
            - datetime: datetime
            - timelinux: pd.to_datetime(df['datetime']).astype('int64')/10**18
            - t: temperature
        tref: reference temperature
    """
    integrales = [0]
    test_fechas = [0]
    df["datetime"] = pd.to_datetime(df["datetime"])
    #for grupo in df.groupby(df.datetime.dt.date)[['datetime', 't']]:
    fechas = df['datetime'].to_numpy()
    #x = grupo[1]['timelinux'].to_numpy()  # cada grupo es una tupla de dos elementos, 
    y = df['t'].to_numpy()          # en la pos 1 está el dataframe de cada grupo
    # número de trapecios
    n = len(y) - 1
    
    for i in range(n):
        delta_fechas  = fechas[i+1] - fechas[i]
        delta_minutos = (delta_fechas/ np.timedelta64(1, 's'))/ 60
        delta_hora    = delta_minutos/60
        delta_dia     = delta_hora/24
        test_fechas.append(delta_dia)
        tavg = ((y[i+1] + y[i]) / 2 ) - tref
        if tavg < 0:
            tavg = 0
        area_i = (delta_dia) * tavg
        integrales.append(area_i)
            
    df["gdd_fino_v3"]  = np.array(integrales)
    df["gdd_acum_v3"]  = df["gdd_fino_v3"].cumsum()
    df["delta_dia_v3"] = np.array(test_fechas)
    df["delta_dia_acum_v3"] = df["delta_dia_v3"].cumsum()
    return df

def get_daily_gdd(df):
    """compute daily GDD
    Input:
        - df: dataframe with datetime and gdd_fino as columns
    Output
        - a dataframe with datetime (yyyy-mm-dd) and gdd columns"""
    daily_gdd = [grupo[1]['gdd_fino'].sum() for grupo in df.groupby(df.datetime.dt.date)[["datetime", "gdd_fino"]]]
    dates = df.datetime.dt.date.unique()
    df_out = pd.DataFrame({"date":pd.to_datetime(dates), "gdd":daily_gdd})
    return df_out

    