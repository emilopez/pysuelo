from scipy.stats import norm
import statsmodels.api as sm

import numpy as np
import pandas as pd


def get_cycle(df, ini, end, eq="4"):
    """prepare dataframe with the correct structure
    
        ini: begin of the cycle (datetime.datetime(yyyy,m,d))
        end: end of the cycle (datetime.datetime(yyyy,m,d))
        eq: device used
        
        output columns: datetime, timelinux, s1, s2, s3, s#..., temp
    """
    df['timelinux'] = pd.to_datetime(df['datetime']).astype('int64')/10**18
    idx = ((df.datetime > ini) & (df.datetime < end))
    df = df[idx]
    if eq == "4":
        s1 = df[df["sensor"] == "1"]
        s2 = df[df["sensor"] == "2"]
        s3 = df[df["sensor"] == "3"]
        s4 = df[df["sensor"] == "4"]
        s5 = df[df["sensor"] == "5"]
        ht = df[df["sensor"] == "ht"]
        v = df[df["sensor"] == "v"]
    
        # me quedo con las columnas utiles
        s1 = s1[["datetime", "timelinux", "d1"]]
        s2 = s2[["datetime", "timelinux", "d1"]]
        s3 = s3[["datetime", "timelinux", "d1"]]
        s4 = s4[["datetime", "timelinux", "d1"]]
        s5 = s5[["datetime", "timelinux", "d1"]]
        ht = ht[["datetime", "timelinux", "d1", "d2"]]
    
        # renombro columas
        s1.rename(columns={"d1":"s1"}, inplace=True)
        s2.rename(columns={"d1":"s2"}, inplace=True)
        s3.rename(columns={"d1":"s3"}, inplace=True)
        s4.rename(columns={"d1":"s4"}, inplace=True)
        s5.rename(columns={"d1":"s5"}, inplace=True)
        ht.rename(columns={"d1":"h", "d2":"t"}, inplace=True)
    
        # agrego columnas diferentes en los otros dataframes a s1, en s1 armo el DF final
        s1["s2"] = s2["s2"].tolist()
        s1["s3"] = s3["s3"].tolist()
        s1["s4"] = s4["s4"].tolist()
        s1["s5"] = s5["s5"].tolist()
        s1["t"] = ht["t"].tolist()

    if eq == "B4":
        s1 = df[df["sensor"] == "1"]
        s2 = df[df["sensor"] == "2"]
        s3 = df[df["sensor"] == "3"]
        ht = df[df["sensor"] == "ht"]
        v = df[df["sensor"] == "v"]
    
        # me quedo con las columnas utiles
        s1 = s1[["datetime", "timelinux", "d1"]]
        s2 = s2[["datetime", "timelinux", "d1"]]
        s3 = s3[["datetime", "timelinux", "d1"]]
        ht = ht[["datetime", "timelinux", "d1", "d2"]]
    
        # renombro columas
        s1.rename(columns={"d1":"s1"}, inplace=True)
        s2.rename(columns={"d1":"s2"}, inplace=True)
        s3.rename(columns={"d1":"s3"}, inplace=True)
        ht.rename(columns={"d1":"h", "d2":"t"}, inplace=True)
    
        # agrego columnas diferentes en los otros dataframes a s1, en s1 armo el DF final
        s1["s2"] = s2["s2"].tolist()
        s1["s3"] = s3["s3"].tolist()
        s1["t"] = ht["t"].tolist()
    
    return s1

def compute_envelope(hsensors, cycle, sensors="s1 s2 s3", window=30):
    """compute envelope and save in a dict(): {"s1": [], "s2":...}
            hsensors: sensor's height
            cycle: dataframe with growth cycle data
            sensors: dataframe columns name of each sensor
            """
    
    z = norm.ppf(0.95) 
    envelope = dict()
    for sensor in sensors.split():
        # medias y desvío estándar móvil
        rolling_mean = (hsensors - cycle[sensor]).rolling(window=window, min_periods=1).mean()
        rolling_std  = (hsensors - cycle[sensor]).rolling(window=window, min_periods=1).std()
    
        # Calcula la envolvente superior en cada punto de datos
        env_sup = rolling_mean + z * rolling_std
        envelope[sensor] = env_sup
    return envelope
        
def compute_lowess(cycle, envelope, sensors="s1 s2 s3", frac=1./11):
    """compute smoothing using lowess filter"""
    lowess = sm.nonparametric.lowess
    
    flowess = dict()
    # aplico filtro de lowess a cada envolvente
    for i, sensor in enumerate(sensors.split()):
        idx = envelope[sensor].notna()
        y = envelope[sensor].loc[idx]
        x = cycle["timelinux"].loc[idx]
        # filtro lowess
        flowess[sensor] = lowess(y, x, is_sorted=True, frac=frac)
        #lowess(y, x, frac= 1./3, it=0)
    return flowess

def clap(cycle, sensors="s1 s2 s3", hmax=70, h_sensor=125):
    """NaN for those values that are above the threshold hmax"""
    for sensor in sensors.split():
        idx = (((h_sensor-cycle[sensor]) > hmax))
        cycle[idx] = np.nan
        cycle.dropna(inplace=True)
    return cycle

def normalize(x,y):
    """Normalize x and y variable"""
    
    # voy a normalizar para no tener datos negativos: [0, 1]
    x_min, x_max = np.min(x), np.max(x)
    y_max = np.max(y)
    
    x_norm = (x - x_min) / (x_max - x_min)
    y_norm = y / y_max
    
    return x_norm, y_norm