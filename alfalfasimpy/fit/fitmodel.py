import numpy as np


def logistic1(x, a, b, c, d):
    """
    Función logistica:
    a: máximo valor de la curva
    b: offset
    c: tasa de crecimiento 
    d: valor de x en el punto medio de la curva sigmoidal
    """
    return (a / (1 + np.exp(-c * (x - d)))) + b

def logistic1b(x, b, c, d):
    """
    Función logistica:
    : máximo valor de la curva 1
    b: offset
    c: tasa de crecimiento 
    d: valor de x en el punto medio de la curva sigmoidal
    """
    return (1 / (1 + np.exp(-c * (x - d)))) + b

def logistic2(x, h, b0, b1):
    return h / (1 + b0 * np.exp(-b1 * x))


def logistic3(x, a, b, c):
    return c / (1 + np.exp(-(x - b) / a))


def landau(x, Hu, n):
    """
    Eq. Landau
    """
    return np.sqrt(0.5 + 0.5*np.tanh(Hu*x - n))

def mytanh(x, Hu, n):
    """
    CV, equivalente a mylogistic
    """
    return 0.5 + 0.5*np.tanh(Hu*x - n)

def mylogistic(x, beta, Hu):
    """
    CV, equivalente a mytanh
    """
    return 1 / (1 + beta*np.exp(-Hu*x))

def gompertz(x, c1, c2):
    """
    CV, equivalente a mytanh
    """
    return np.exp(-c1*np.exp(-c2*x))