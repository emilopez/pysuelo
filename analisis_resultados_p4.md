# Calibración automática de sensores de humedad de suelo mediante gravimetría

## Descripción general del sistema

Se montó un sistema de sensado de humedad de suelo y captura fotográfica automática del pesaje de balanzas. Posteriormente, se desarrolló un software OCR que reconoce el valor numérico del display a partir de las fotografías tomadas y finalmente con otro software los combina con el de los sensores.

Cada 15 minutos se toma una foto y a su vez se leen los sensores en una ráfaga de 30 mediciones. A continuación se muestra un diagrama del sistema de adquisición:

<img src="img/diag/sistema.png" alt="Sistema" title="Sistema de captura" width="500" />


A continuación se observa el montaje del sistema diseñado:

<img src="fotos/sensores_capacitivos/sistema_captura.jpg" alt="Sistema" title="Sistema de captura" width="350" /> 
<img src="fotos/sensores_capacitivos/sistema_captura2.jpg" alt="Sistema" title="Sistema de captura" width="350" />
<img src="fotos/sensores_capacitivos/sistema_captura3.jpg" alt="Sistema" title="Sistema de captura" width="350" />


## Preparación de sensores

Se protegió la electrónica del sensor mediante aislante acrílico y silicona, se probó el rango de valores al aire y completamente sumergidos en agua.

<img src="fotos/sensores_capacitivos/IMG_20200211_133940850.jpg" alt="Sistema" title="Sistema de captura" width="230" />
<img src="fotos/sensores_capacitivos/IMG_20200211_151527157.jpg" alt="Sistema" title="Sistema de captura" width="380" />
<img src="fotos/sensores_capacitivos/IMG_20200211_153942416.jpg" alt="Sistema" title="Sistema de captura" width="380" />

- Se puede observar los videos del proceso de secado en los siguientes enlaces:
  - https://youtu.be/8Ddt2xBfa2o
  - https://youtu.be/Dlm4kR4mXh4

## Pre-Procesamiento

- Los datos de las fotos son cada 15 minutos: "data/lectura_balanzas_p4.csv"
- Los sensores toman varias lecturas cada 15 minutos: "data/hum_suelo_datos_completos_p4.csv"

Salida: **data/balanzas_y_sensores_p4.csv**


```python

```

Con este fragmento hice un merge de ambos archivos:

- Me paro en una fecha de la foto
- Busco en el archivo de sensores la primera ocurrencia de esa fecha y hora y la retorno

```python
balanzas = open("data/lectura_balanzas_p4.csv")
balanzas.readline()
header = "datetime_b,b1,b2,datetime_s,hum_dhtA,hum_dhtB,hum_dhtC,tem_dhtA,tem_dhtB,tem_dhtC,tem_shtA,tem_shtB,hum_shtA,hum_shtB,c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10"
fout = open("data/balanzas_y_sensores_p4.csv", "w")
fout.write(header)
fout.write("\n")

def get_data_at_time(dt):
    sensores = open("data/hum_suelo_datos_completos_p4.csv")
    _ = sensores.readline()
    dato_encontrado = False
    for dato_sensores in sensores:
        dtsensores = dato_sensores[:16]
        if dt == dtsensores:
            dato_encontrado = True
            dato =  dato_sensores.strip()
            break
    if dato_encontrado:
        return dato.strip()
    else:
        return "Nan"
    
    
for dato_balanza in balanzas:
    dt, b1, b2 = dato_balanza.split(",")
    data_sensores = get_data_at_time(dt)
    new_line = f"{dt},{b1},{b2.strip()},{data_sensores}"
    fout.write(new_line)
    fout.write("\n")
fout.close()

```

## Gráficas de dispersión entre valores balanzas y sensores

Vamos hacer una regresión lineal entre cada sensor y lecturas de las balanzas

- Balanza 1: c0, c1, c2, c3, c4, c5
- Balanza 2: c6, c7, c8, c9, c10


```python
import pandas as pd 
import numpy as np 
import plotly.graph_objects as go
```


```python
datos = pd.read_csv("data/balanzas_y_sensores_p4.csv", sep=",", parse_dates=['datetime_b', "datetime_s"])
```

### Vamos a trabajar con la primer curva de secado solamente
- Las primeras 2572 filas


```python
datos = datos[:2572]
```


```python
datos.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>datetime_b</th>
      <th>b1</th>
      <th>b2</th>
      <th>datetime_s</th>
      <th>hum_dhtA</th>
      <th>hum_dhtB</th>
      <th>hum_dhtC</th>
      <th>tem_dhtA</th>
      <th>tem_dhtB</th>
      <th>tem_dhtC</th>
      <th>...</th>
      <th>c1</th>
      <th>c2</th>
      <th>c3</th>
      <th>c4</th>
      <th>c5</th>
      <th>c6</th>
      <th>c7</th>
      <th>c8</th>
      <th>c9</th>
      <th>c10</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2020-02-14 15:45:00</td>
      <td>3676.5</td>
      <td>3626</td>
      <td>2020-02-14 15:45:00</td>
      <td>48.7</td>
      <td>53.2</td>
      <td>47.9</td>
      <td>25.7</td>
      <td>25.9</td>
      <td>26.1</td>
      <td>...</td>
      <td>268.0</td>
      <td>257.0</td>
      <td>246.0</td>
      <td>306.0</td>
      <td>81.0</td>
      <td>296</td>
      <td>263.0</td>
      <td>274.0</td>
      <td>267</td>
      <td>273</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2020-02-14 16:00:00</td>
      <td>3676.6</td>
      <td>3624</td>
      <td>2020-02-14 16:00:00</td>
      <td>47.0</td>
      <td>51.0</td>
      <td>46.2</td>
      <td>25.5</td>
      <td>25.6</td>
      <td>25.7</td>
      <td>...</td>
      <td>270.0</td>
      <td>261.0</td>
      <td>248.0</td>
      <td>309.0</td>
      <td>81.0</td>
      <td>298</td>
      <td>266.0</td>
      <td>276.0</td>
      <td>269</td>
      <td>276</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2020-02-14 16:15:00</td>
      <td>3676.4</td>
      <td>3624</td>
      <td>2020-02-14 16:15:00</td>
      <td>46.9</td>
      <td>50.8</td>
      <td>46.1</td>
      <td>25.5</td>
      <td>25.6</td>
      <td>25.7</td>
      <td>...</td>
      <td>268.0</td>
      <td>260.0</td>
      <td>248.0</td>
      <td>309.0</td>
      <td>81.0</td>
      <td>298</td>
      <td>266.0</td>
      <td>276.0</td>
      <td>269</td>
      <td>275</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2020-02-14 16:30:00</td>
      <td>3677.3</td>
      <td>3623</td>
      <td>2020-02-14 16:30:00</td>
      <td>46.7</td>
      <td>50.1</td>
      <td>45.3</td>
      <td>23.3</td>
      <td>23.4</td>
      <td>23.8</td>
      <td>...</td>
      <td>268.0</td>
      <td>257.0</td>
      <td>246.0</td>
      <td>304.0</td>
      <td>80.0</td>
      <td>296</td>
      <td>262.0</td>
      <td>274.0</td>
      <td>267</td>
      <td>273</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2020-02-14 16:45:00</td>
      <td>3677.3</td>
      <td>3623</td>
      <td>2020-02-14 16:45:00</td>
      <td>49.0</td>
      <td>53.1</td>
      <td>47.7</td>
      <td>23.6</td>
      <td>23.7</td>
      <td>24.0</td>
      <td>...</td>
      <td>272.0</td>
      <td>260.0</td>
      <td>248.0</td>
      <td>307.0</td>
      <td>80.0</td>
      <td>298</td>
      <td>265.0</td>
      <td>276.0</td>
      <td>269</td>
      <td>275</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 25 columns</p>
</div>



### Valores crudos


```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.c0, y=datos.b1, mode='markers', name="c0"))
fig.add_trace(go.Scattergl(x=datos.c1, y=datos.b1, mode='markers', name="c1"))
fig.add_trace(go.Scattergl(x=datos.c2, y=datos.b1, mode='markers', name="c2"))
fig.add_trace(go.Scattergl(x=datos.c3, y=datos.b1, mode='markers', name="c3"))
fig.add_trace(go.Scattergl(x=datos.c4, y=datos.b1, mode='markers', name="c4"))
#fig.add_trace(go.Scattergl(x=datos.c5, y=datos.b1, mode='markers', name="c5"))
fig.update_layout(title='Sensores y Balanza 1', xaxis_title='sensor capacitivo',yaxis_title='Peso Balanza 1')
fig.show()
```


![png](img/resultados_p4/output_12_0.png)



```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.c6, y=datos.b2, mode='markers', name="c6"))
fig.add_trace(go.Scattergl(x=datos.c7, y=datos.b2, mode='markers', name="c7"))
fig.add_trace(go.Scattergl(x=datos.c8, y=datos.b2, mode='markers', name="c8"))
fig.add_trace(go.Scattergl(x=datos.c9, y=datos.b2, mode='markers', name="c9"))
fig.add_trace(go.Scattergl(x=datos.c10, y=datos.b2, mode='markers', name="c10"))
fig.update_layout(title='Sensores y Balanza 2', xaxis_title='sensor capacitivo',yaxis_title='Peso Balanza 2')
fig.show()
```


![png](img/resultados_p4/output_13_0.png)


Código para guardar imágenes sucesivas de las curvas de secado y luego hacer la animación

```python
import matplotlib.pyplot as plt
j = 0
for i in range(0,len(datos)-1,100):
    plt.scatter(x=datos[0:i].c0, y=datos[0:i].b1, alpha=1, c="blue", s=1)
    plt.title("Gravimetria / Sensor c0")
    plt.xlim(200, 500)
    plt.ylim(3100, 3800)
    plt.xlabel("valor del sensor")
    plt.ylabel("peso balanza 1[gr]")
    plt.savefig(f"sensorc0_{i}.png")

# Luego hice el video con:
#ffmpeg -pattern_type glob -i '*.png' -vf "fps=200,format=yuv420p" 2020-02-14_2020-04-05.mp4
```


### Conversión a contenido volumétrico de agua

- **Datos iniciales:**

|                                          | Envase 1                           | Envase 2         |
|------------------------------------------|------------------------------------|------------------|
| Balanza                                  | Ohaus Scout Pro                    | Ohaus CS Serie   |
| Sensores capacitivos                     | v2.0                               | v1.2             |
| ID sensores                              | shtA, shtB, c0, c1, c2, c3, c4, c5 | c6, c7,c8,c9,c10 |
| Peso                                     | 128 gr                             | 128 gr           |
| Peso de arena                            | 2872 gr                            | 2872 gr          |
| Peso de agua                             |  500 gr                            |  500 gr          |
| Altura de arena                          |   9.4  cm                          | 9.4 cm           |
|  Peso Envase (E) + Tierra (T) + Agua (A) |   3500 gr                          |  3500 gr         |
|  Peso E + T + A + sensores               |   3676.6 gr                        |  3625 gr         |
| Timestamp incial                         |   2020-02-14 15:54                 | 2020-02-14 15:54 |

Debemos obtener primeramente el % de agua que tiene cada envase, esto es:

```peso_agua  = balanza - peso_sensores - peso_arena - peso_envase```


```python
peso_envase = 128
peso_arena = 2872
peso_sensores_E1 = 3676.6-3500
peso_sensores_E2 = 3625-3500
densidad_aparente = 1.79
porosidad = 30
```


```python
datos["peso_agua_E1"] = datos["b1"]-peso_sensores_E1 - peso_arena - peso_envase
datos["peso_agua_E2"] = datos["b2"]-peso_sensores_E2 - peso_arena - peso_envase
```


```python
datos["humedad_gravimetrica_E1"] = 100*datos["peso_agua_E1"]/peso_arena
datos["humedad_gravimetrica_E2"] = 100*datos["peso_agua_E2"]/peso_arena
```


```python
datos["humedad_volumetrica_E1"] = datos["humedad_gravimetrica_E1"]*densidad_aparente
datos["humedad_volumetrica_E2"] = datos["humedad_gravimetrica_E2"]*densidad_aparente
```


```python
datos["humedad_E1"] = 100*datos["humedad_volumetrica_E1"]/porosidad
datos["humedad_E2"] = 100*datos["humedad_volumetrica_E2"]/porosidad
```


```python
datos.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>datetime_b</th>
      <th>b1</th>
      <th>b2</th>
      <th>datetime_s</th>
      <th>hum_dhtA</th>
      <th>hum_dhtB</th>
      <th>hum_dhtC</th>
      <th>tem_dhtA</th>
      <th>tem_dhtB</th>
      <th>tem_dhtC</th>
      <th>...</th>
      <th>c9</th>
      <th>c10</th>
      <th>peso_agua_E1</th>
      <th>peso_agua_E2</th>
      <th>humedad_gravimetrica_E1</th>
      <th>humedad_gravimetrica_E2</th>
      <th>humedad_volumetrica_E1</th>
      <th>humedad_volumetrica_E2</th>
      <th>humedad_E1</th>
      <th>humedad_E2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2020-02-14 15:45:00</td>
      <td>3676.5</td>
      <td>3626</td>
      <td>2020-02-14 15:45:00</td>
      <td>48.7</td>
      <td>53.2</td>
      <td>47.9</td>
      <td>25.7</td>
      <td>25.9</td>
      <td>26.1</td>
      <td>...</td>
      <td>267</td>
      <td>273</td>
      <td>499.9</td>
      <td>501</td>
      <td>17.405989</td>
      <td>17.444290</td>
      <td>31.156720</td>
      <td>31.225279</td>
      <td>103.855734</td>
      <td>104.084262</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2020-02-14 16:00:00</td>
      <td>3676.6</td>
      <td>3624</td>
      <td>2020-02-14 16:00:00</td>
      <td>47.0</td>
      <td>51.0</td>
      <td>46.2</td>
      <td>25.5</td>
      <td>25.6</td>
      <td>25.7</td>
      <td>...</td>
      <td>269</td>
      <td>276</td>
      <td>500.0</td>
      <td>499</td>
      <td>17.409471</td>
      <td>17.374652</td>
      <td>31.162953</td>
      <td>31.100627</td>
      <td>103.876509</td>
      <td>103.668756</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2020-02-14 16:15:00</td>
      <td>3676.4</td>
      <td>3624</td>
      <td>2020-02-14 16:15:00</td>
      <td>46.9</td>
      <td>50.8</td>
      <td>46.1</td>
      <td>25.5</td>
      <td>25.6</td>
      <td>25.7</td>
      <td>...</td>
      <td>269</td>
      <td>275</td>
      <td>499.8</td>
      <td>499</td>
      <td>17.402507</td>
      <td>17.374652</td>
      <td>31.150487</td>
      <td>31.100627</td>
      <td>103.834958</td>
      <td>103.668756</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2020-02-14 16:30:00</td>
      <td>3677.3</td>
      <td>3623</td>
      <td>2020-02-14 16:30:00</td>
      <td>46.7</td>
      <td>50.1</td>
      <td>45.3</td>
      <td>23.3</td>
      <td>23.4</td>
      <td>23.8</td>
      <td>...</td>
      <td>267</td>
      <td>273</td>
      <td>500.7</td>
      <td>498</td>
      <td>17.433844</td>
      <td>17.339833</td>
      <td>31.206581</td>
      <td>31.038301</td>
      <td>104.021936</td>
      <td>103.461003</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2020-02-14 16:45:00</td>
      <td>3677.3</td>
      <td>3623</td>
      <td>2020-02-14 16:45:00</td>
      <td>49.0</td>
      <td>53.1</td>
      <td>47.7</td>
      <td>23.6</td>
      <td>23.7</td>
      <td>24.0</td>
      <td>...</td>
      <td>269</td>
      <td>275</td>
      <td>500.7</td>
      <td>498</td>
      <td>17.433844</td>
      <td>17.339833</td>
      <td>31.206581</td>
      <td>31.038301</td>
      <td>104.021936</td>
      <td>103.461003</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 33 columns</p>
</div>




```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.datetime_b, y=datos["humedad_E1"], mode='markers', name="Balanza 1"))
fig.add_trace(go.Scattergl(x=datos.datetime_b, y=datos["humedad_E2"], mode='markers', name="Balanza 2"))

fig.update_layout(title='% Humedad a partir del peso', xaxis_title='fecha',yaxis_title='% humedad')
fig.show()
```


![png](img/resultados_p4/output_24_0.png)



```python

```


```python

```


```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.c0, y=datos["humedad_E1"], mode='markers', name="c0 (E1)"))
fig.add_trace(go.Scattergl(x=datos.c1, y=datos["humedad_E1"], mode='markers', name="c1 (E1)"))
fig.add_trace(go.Scattergl(x=datos.c2, y=datos["humedad_E1"], mode='markers', name="c2 (E1)"))
fig.add_trace(go.Scattergl(x=datos.c3, y=datos["humedad_E1"], mode='markers', name="c3 (E1)"))
fig.add_trace(go.Scattergl(x=datos.c4, y=datos["humedad_E1"], mode='markers', name="c4 (E1)"))
#fig.add_trace(go.Scattergl(x=datos.c5, y=datos["humedad_E1"], mode='markers', name="c5 (E1)"))
fig.add_trace(go.Scattergl(x=datos.c6, y=datos["humedad_E2"], mode='markers', name="c6 (E2)"))
fig.add_trace(go.Scattergl(x=datos.c7, y=datos["humedad_E2"], mode='markers', name="c7 (E2)"))
fig.add_trace(go.Scattergl(x=datos.c8, y=datos["humedad_E2"], mode='markers', name="c8 (E2)"))
fig.add_trace(go.Scattergl(x=datos.c9, y=datos["humedad_E2"], mode='markers', name="c9 (E2)"))
fig.add_trace(go.Scattergl(x=datos.c10, y=datos["humedad_E2"], mode='markers', name="c10 (E2)"))
fig.update_layout(title='% de humedad / sensores', xaxis_title='sensor capacitivo',yaxis_title='% humedad')
fig.show()
```


![png](img/resultados_p4/output_27_0.png)


## Humedad y termperatura ambiente

Son llamativos los períodos donde **los tres** sensores de temperatura y humedad ambiente DHT22 fallan. Acá estoy visualizando la primera medida de cada sensor, deberían analizarse las subsiguientes, ya que el registro era por ráfagas de 30 medidas.



```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["hum_dhtA"], mode='markers', name="HR (A)"))
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["hum_dhtB"], mode='markers', name="HR (B)"))
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["hum_dhtC"], mode='markers', name="HR (C)"))

fig.update_layout(title='Humedad Relativa (HR) / dht22', xaxis_title='fecha',yaxis_title='Humedad (%)')
fig.show()
```


![png](img/resultados_p4/output_29_0.png)



```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["tem_dhtA"], mode='markers', name="T (A)"))
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["tem_dhtB"], mode='markers', name="T (B)"))
fig.add_trace(go.Scattergl(x=datos.datetime_s, y=datos["tem_dhtC"], mode='markers', name="T (C)"))

fig.update_layout(title='Temperatura ambiente / dht22', xaxis_title='fecha',yaxis_title='Temperatura (°C)')
fig.show()
```


![png](img/resultados_p4/output_30_0.png)


Miro de cerca los datos llamativos


```python
fallas = pd.read_csv("data/hum_suelo_datos_completos_p4_fallasDHT22.CSV", sep=",", parse_dates=['datetime'])
```


```python
fig = go.Figure()
fig.add_trace(go.Scattergl(x=fallas.datetime, y=fallas["tem_dhtA"], mode='markers+lines', name="T(A)"))
fig.add_trace(go.Scattergl(x=fallas.datetime, y=fallas["tem_dhtB"], mode='markers+lines', name="T(B)"))
fig.add_trace(go.Scattergl(x=fallas.datetime, y=fallas["tem_dhtC"], mode='markers+lines', name="T(C)"))

fig.update_layout(title='Temperatura ambiente / dht22', xaxis_title='fecha',yaxis_title='Temperatura (°C)')
fig.show()
```


![png](img/resultados_p4/output_33_0.png)



```python

```
