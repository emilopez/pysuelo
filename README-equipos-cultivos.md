# Configuración de equipos de medición agro-hidrológicos

[Versión online de este documento](https://gitlab.com/emilopez/pysuelo/-/blob/master/README-equipos-cultivos.md)

Se diseñaron cuatro equipos de medición de diferentes variables agro-hidrológicas para ser instalados en Esperanza. 

- **Equipo 1**: Nivel freático, humedad y temperatura ambiente
- **Equipo 2**: Humedad de suelo, humedad y temperatura ambiente
- **Equipo 3**: Precipitación, humedad y temperatura ambiente
- **Equipo 4**: Crecimiento de 5 parcelas de cultivo, humedad y temperatura ambiente

<img src="img/equipos/eq4_alfalfa_casaguille.jpg" alt="eq4 crecimiento cultivos" title="alfalfa" width="150"/>
<img src="img/equipos/eq1_nf_casaguille.jpg" alt="eq1 nivel freatico" title="nf" width="150"/>
<img src="img/equipos/eq_cableadointerno.jpg" alt="coneccionado" title="cableado interno 1" width="150"/>
<img src="img/equipos/eq_cableadointerno2.jpg" alt="coneccionado" title="cableado interno 2" width="150"/>			

Los códigos fuente de cada equipo se encuentran publicados [aquí](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino)

## Esquemático

Tenemos dos diseños de equipos, uno con único voltaje y otro con doble voltaje. 

El esquema de voltaje único se usa para los equipos 1 (NF), 3 (Pluvio/anemómetro) y 4 (crecimiento de cultivos).

<img src="img/conexionado-equipos/esquema-circuito-dloggers-voltaje-unico.png" alt="voltaje unico" title="voltaje unico" width="600"/>

El esquema de dos voltajes se usa para el **equipo 2** (Humedad de suelo) ya que el sensor hydraprobe funciona entre 9 a 12v. Este sensor se encuentra todo el tiempo alimentado, no tiene ahorro energético debido a su bajo consumo (<1mA>)

<img src="img/conexionado-equipos/esquema-circuito-dloggers-voltaje-doble.png" alt="doble voltaje" title="doble voltaje" width="600"/>

Todos los equipos cuentan con un pulsador normal abierto que permite monitorear la medición en tiempo real. A continuación se muestra el conexionado.

<img src="img/conexionado-equipos/esquema-circuito-monitor.png" alt="monitor" title="monitor" width="250"/>

Por último, el pluviómetro cuenta con un circuito extra para detectar cada cangilón (reed switch). En este caso se lo detecta cambiando el estado a bajo utilizando el siguiente circuito.

<img src="img/conexionado-equipos/esquema-circuito-pluvio.png" alt="circuito pluvio" title="circuito pluvio" width="250"/>

## Equipo 1: Nivel Freático

- Componentes: datalogger, display oled, dht22, sonda nivel freático
- [Link al código fuente](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/eq1_NF)

### Funcionamiento

Registra en archivo aaaammdd.CVS

- timestamp (10,11,12,13)
- voltaje (A6)
- temp y hum ambiente (D8)
- Nivel freático (5, 6, 7)

### Conexionado Sensor - UTP

<img src="img/conexionado_honeywell/conn_honey.png" alt="coneccionado" title="honeywell" width="350"/>



|        Sensor     |  UTP            | Bornera enchufable|
|-------------------|-----------------|-------------------|
| CS   transparente | blanco/celeste  | 5 naranja         |
| CLK  naranja      | blanco/marron   | 6 azul            |
| MISO amarillo     | verde           | 7 verde           |
| GND  celeste      | azul            | rojo              |
| VCC  lila         | marron          | negro             |


### Conexionado PCB Arduino

| PIN   | USO           |
|-------|---------------|
| A0    |  MONITOR BTN  |
| A1    |               |
| A2    |               |
| A3    |               |
| A4    | SDA RTC, OLED |
| A5    | SCL RTC, OLED |
| A6    | VOLTAJE       |
| A7    |               |
| Rx D0 | x             |
| Tx D1 | x             |
| D2    |               |
| D3    |               |
| D4    |               |
| D5    | CS   HONEY    |
| D6    | SCLK HONEY    |
| D7    | MISO HONEY    |
| D8    | DHT22         |
| D9    | LLAVE         |
| D10   | CS SD         |
| D11   | MOSI RTC      |
| D12   | MISO RTC      |
| D13   | SCLK RTC      |
| 5V    |PULSADOR -> A0 |


## Equipo 2: Humedad de Suelo

- Componentes: datalogger, display oled, dht22, hydraprobe (h1), 3 capacitivos (c7, c8, c9), panel 12v
- Alimenta hidraprobe con 12V y resto de los sensores con 5V
- [Link al código fuente](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/eq2_HS)

### Funcionamiento

Registra en archivo aaaammdd.CVS 17 datos:

- timestamp (pines 10,11,12,13)
- voltaje (A6)
- humedad ambiente (D4)
- temperatura 
- hydraprobe (D8): 10 datos
- sensor capacitivo c7 
- sensor capacitivo c8
- sensor capacitivo c9

La cadena del hydraproble son 10 valores: ``id;soil_moisture;conductivity_corrected;tempC;tempF;conductivity_raw;r_permittivity;i_permittivity;r_permittivity_corrected;i_permittivity_corrected``

### Conexionado

| PIN   | USO           |
|-------|---------------|
| A0    | CAPACITIVO7   |
| A1    | CAPACITIVO8   |
| A2    | CAPACITIVO9   |
| A3    | MONITOR BTN   |
| A4    | SDA RTC, OLED |
| A5    | SCL RTC, OLED |
| A6    |    VOLTAJE    |
| A7    |               |
| Rx D0 | x             |
| Tx D1 | x             |
| D2    |               |
| D3    |               |
| D4    |    DHT22      |
| D5    |               |
| D6    |               |
| D7    |               |
| D8    |  HYDRAPROBE   |
| D9    |   LLAVE       |
| D10   |   CS SD       |
| D11   |   MOSI SD     |
| D12   |   MISO SD     |
| D13   |   SCLK SD     |
| 5V    | PULSADOR-> A3 |

## Equipo 3: Pluviometro y Anemometro

- Componentes: datalogger, display oled, dht22, pluviometro
- [Link al código fuente](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/eq3_PLUVIO)

### Funcionamiento

Registra en archivo aaaammdd.CVS

- timestamp (pines 10,11,12,13)
- voltaje (A1)
- temp y hum ambiente (D4)
- BOTON MONITOR (A0)
- PLUVIO CANGILON (D2)

### Conexionado

| PIN   | USO           |
|-------|---------------|
| A0    | MONITOR BTN   |
| A1    |               |
| A2    | ANEMOMETRO    |
| A3    |               |
| A4    | SDA RTC, OLED |
| A5    | SCL RTC, OLED |
| A6    | VOLTAJE       |
| A7    |               |
| Rx D0 | x             |
| Tx D1 | x             |
| D2    | ISR PLUVIO    |
| D3    |               |
| D4    | DHT22         |
| D5    |               |
| D6    |               |
| D7    |               |
| D8    |               |
| D9    | LLAVE         |
| D10   | CS SD         |
| D11   | MOSI SD       |
| D12   | MISO SD       |
| D13   | SCLK SD       |
| 5V    | PULSADOR->A0  |
| GND   | REED SWITCH  -> D2 |

## Equipo 4: Crecimiento Alfalfa

- Componentes: datalogger, display oled, dht22, 5 sensores acústicos MB7092
- [Link al código fuente](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/eq4_ALFALFA)

### Funcionamiento

Registra en archivo aaaammdd.CVS

- timestamp
- voltaje (A6)
- 10 dist1 (A0)
- 10 dist2 (A1)
- 10 dist3 (A2)
- 10 dist4 (A3)
- 10 dist5 (A7)
- temp y hum ambiente (D7)

### Conexionado

| PIN   | USO           |
|-------|---------------|
| A0    | ACUSTICO1     |
| A1    | ACUSTICO2     |
| A2    | ACUSTICO3     |
| A3    | ACUSTICO4     |
| A4    | SDA RTC, OLED |
| A5    | SCL RTC, OLED |
| A6    | VOLTAJE       |
| A7    | ACUSTICO5     |
| RX D0 |    x          |
| Tx D1 |    x          |
| D2    |               |
| D3    |               |
| D4    |               |
| D5    |               |
| D6    |               |
| D7    | DHT22         |
| D8    | MONITOR BTN, PULLDOWN GND   |
| D9    | LLAVE         |
| D10   | CS SD         |
| D11   | MOSI SD       |
| D12   | MISO SD       |
| D13   | SCLK SD       |
| 5V    | PULSADOR->D8  |
|       |               |

## Equipo 5: Humedad de Suelo

- Componentes: datalogger, display oled, dht22, hydraprobe (h2), hydraprobe (h4), panel 12v
- Alimenta los hydraprobe con 12V (sin corte) y el DHT22 con 5V (con corte)
- [Link al código fuente](https://gitlab.com/emilopez/pysuelo/-/tree/master/src/arduino/eq2_HS)

### Funcionamiento

Registra en archivo aaaammdd.CVS 17 datos:

- timestamp (pines 10,11,12,13)
- voltaje (A6)
- humedad ambiente (D4)
- temperatura 
- hydraprobe h2 (D8): 10 datos
- hydraprobe h4 (D8): 10 datos

La cadena del hydraproble son 10 valores: ``id;soil_moisture;conductivity_corrected;tempC;tempF;conductivity_raw;r_permittivity;i_permittivity;r_permittivity_corrected;i_permittivity_corrected``

### Conexionado

| PIN   | USO           |
|-------|---------------|
| A0    | CAPACITIVO7   |
| A1    | CAPACITIVO8   |
| A2    | CAPACITIVO9   |
| A3    | MONITOR BTN   |
| A4    | SDA RTC, OLED |
| A5    | SCL RTC, OLED |
| A6    |    VOLTAJE    |
| A7    |               |
| Rx D0 | x             |
| Tx D1 | x             |
| D2    |               |
| D3    |               |
| D4    |    DHT22      |
| D5    |               |
| D6    | HYDRAPROBE h4 |
| D7    |               |
| D8    | HYDRAPROBE h2 |
| D9    |   LLAVE       |
| D10   |   CS SD       |
| D11   |   MOSI SD     |
| D12   |   MISO SD     |
| D13   |   SCLK SD     |
| 5V    | PULSADOR-> A3 |