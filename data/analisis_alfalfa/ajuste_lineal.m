% ajuste_lineal.m : ajusta una recta a datos observ
    clf
    filename='mean_values.dat'
    fid  = fopen(filename,'r');
    [v1]= dlmread(fid)
    t  = v1(:,1);  % asigna columna 1
    c  = v1(:,2); % asigna columna 2
    c1= 1./c;
    m = length( t );
    for i=1:m
          printf('%10.4f %12.6f %12.6f\n', t(i), c(i), c1(i))
    end
    fclose(fid);
% arma el sistema de ecuac.
    disp('d=')
    d   = log(log(c1));
    disp(d);
% plotting
    scatter (t, d,'r','filled')
    title('Ajuste por Minimos Cuadrados')
    xlabel('t_i'); ylabel('log(log(y_i)')
%  axis([0. 6. -3 3])
    hold on;
    A(1:m,1)=  1;
    A( : ,   2)= - t;
    A
    B= A'*A    ; e= A'*d;
    B
    e
% ajuste por minimos cuadr, c=x(1); d=x(2)
    x= B \ e
    z= x(1) - x(2)*t;
% plotting
    plot(t, z, 'linewidth', 3, 'b');
    figure;
    B1 = exp(x(1))
    B2 = x(2)
    yaprox = exp(-B1*exp(-B2*t));
    plot(t,c, 'linewidth', 3, 'r',t,yaprox, 'linewidth', 3, 'b')
