% plot_gompertz.m    ! map [0,1] onto [0,GDDmax]
  clc
  clf
  s = 16;                   % size of scatter marker

  filename='pepe.dat'
  c1= 0.79387E+00   ;  c2= 0.34998E+01
  fid= fopen(filename,'r');
  [v]= dlmread(fid)
  t  = v(:,1); % asigna columna 1 - gdd_adim
  y  = v(:,2); % asigna columna 2 -   h_adim
  disp(t);  disp(y);
  fclose(fid);
  
% plotting
  scatter (t,y,s,'k','filled')
  hold on
% myfit
  plot(t, exp(-exp(c1-c2.*t)), 'linewidth', 2, '.-b');
  hold on
% emi's fit
  c1= log(2.24075);  c2= 3.52851;
  plot(t, exp(-exp(c1-c2.*t)), 'linewidth', 2, '-g');
% linear fit
  c1= 0.8136 ;   c2= 3.5285 
  plot(t, exp(-exp(c1-c2.*t)), 'linewidth', 2, '--r');
  legend('mean val','carlos-chi2','emi','linear','location','southeast')