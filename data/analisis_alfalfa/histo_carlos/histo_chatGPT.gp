# Set terminal and output settings
#set terminal push  # Save the current terminal settings
#set output "$0"    # Set the output filename to interactive mode

# General plot settings
 set boxwidth 0.22 absolute
 set clip two
 set border 2 front lt black linewidth 1.000 dashtype solid

# Style settings for filled boxes
# set style fill transparent solid 0.5 border lt -1
 set style fill transparent pattern 4 border lt -1

# Axes labels
 set xlabel 'Normalized Residual'
 set ylabel 'Relative Frequency'

# Axes ranges
 set xrange [-4.0:4.0]
 set yrange [0.0:0.5]

# Axes tics
 set xtics 1.0
 set ytics 0.1
 set tics scale 0.5

# Gaussian distribution function
 f(x) = exp(-x*x/2) / (sqrt(2*pi))

# Plot data
# 4 columns data:
 plot "bucket4.dat" u 1:3:2:xtic(1) with boxes fs solid 0.4 lc rgb "gold"         title "y_{mean}", \
      "bucket4.dat" u 1:4:2         with boxes              lc rgb "forest-green" title "Fisher  ", \
      "bucket4.dat" u 1:5:2         with boxes              lc rgb "dark-violet"  title "Landau  ", \
      "bucket4.dat" u 1:6:2         with boxes              lc rgb "#222324"      title "Gompertz", \
       f(x) title 'Gauss' with lines linestyle 2 lw 3
