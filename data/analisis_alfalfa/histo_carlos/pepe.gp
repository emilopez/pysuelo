#set terminal push            # save the current terminal settings
#set output "$0"              # set the output filename to interactive mode
set boxwidth 0.22 absolute
set clip two
set border 2 front lt black linewidth 1.000 dashtype solid
set boxwidth 0.22 absolute
set clip two
# set style fill transparent pattern 4 border #line type
set style fill transparent pattern 4 border lt -1
# set boxwidth 0.99 relative
# set style fill solid 1.0
# Axes label
set xlabel 'Normalized Residual'
set ylabel 'Relative Frequency'
# Axes ranges
set xrange [-4.0:4.0]
set yrange [ 0.0:0.5]
# Axes tics
#  set xtics offset -1, graph 0.05
  set xtics 1.0
  set ytics 0.1
set tics scale 0.5
f(x)= exp(-x*x/2)/(sqrt(2*pi))
# Plot
#  plot "bucket3.dat" using 2:6 title 'Column' with linespoints 
#  plot "bucket3.dat" using 1:6:2:xtic(1) with boxes notitle
 plot "bucket4.dat" u 1:3:2:xtic(1) with boxes fs solid 0.4 lc rgb "gold"         title "y_{mean}", \
      "bucket4.dat" u 1:4:2         with boxes              lc rgb "forest-green" title "Fisher  ", \
      "bucket4.dat" u 1:5:2         with boxes              lc rgb "dark-violet"  title "Landau  ", \
      "bucket4.dat" u 1:6:2         with boxes              lc rgb "#222324"      title "Gompertz", \
       f(x) title 'Gauss' with lines linestyle 2 lw 3
# set colorbox vertical origin screen 0.9, 0.2 size screen 0.05, 0.6 front  noinvert bdefault
# plot "bucket3.dat" u 1:6:2 with boxes fs solid 1.0 lc rgb "violet"       title "y_{mean}", \
#      "bucket3.dat" u 1:7:2 with boxes              lc rgb "#FF00FF"      title "Fisher", \
#      "bucket3.dat" u 1:8:2 with boxes              palette cb -45        title "Landau", \
#       f(x) title 'Gauss' with lines linestyle 2 lw 3


