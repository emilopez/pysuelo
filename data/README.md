# Archivos de datos 
## Pruebas humedad de suelo
### 1- (HS) Prueba gravimetría exploratoria
- `capacitivo_c2_c3_cuartos.csv` y `capacitivo_c2_c3.csv`: pruebita inicial de valores de dos sensores capacitivos estando secos y mojados y luego parcialmente.
- `hum_suelo_lab_exploratorio_raw.txt` en lab, registrado humedad suelo de dos sensores capacitivos y humedad y temp ambiente con dos sht10, `src/arduino/gravimetria`
- `hum_suelo_lab_exploratorio_clean.txt` obtenido a partir del previo usando `src/py/clean_hum_exploratorio.py`
- `balanza_exploratorio.csv`: lecturas de la balanza en la prueba de los dos archivos previos

### 2- (HS) Prueba exploratoria sensores capacitivos al AIRE y en AGUA

Datos obtenido de todos los sensores estando al aire y sumergidos en agua (solo los capacitivos)

- `hum_suelo_capacitivos_AGUA1.csv`: obtenidos desde el cron (picos no deseados)
- `hum_suelo_capacitivos_AGUA2.csv`: obtenidos desde el cron (picos no deseados)
- `hum_suelo_capacitivos_AGUA3.csv`: obtenidos desde script python conexión abierta (mejoró)
- `hum_suelo_capacitivos_AIRE1.csv`:  obtenidos desde el cron (no se ve picos marcados)
- `hum_suelo_capacitivos_AIRE2.csv`: desde script py conexión abierta cada 5 min

### 3- (HS) Prueba gravimétrica 

- `lectura_balanzas_p3_hs.txt`: datos pasados manualmente de las lectura de las balanzas: timestamp, peso_balanza1, peso_balanza2
- `hum_suelo_raw_p3.txt`: lecturas de los sensores colocados en envase 1 y 2 colocados sobre las balanzas. Ver análisis en `gravimetria_p3.ipynb`.
- `hum_suelo_clean_p3.txt`: solamente renglones con 20 columnas. Ver análisis en `gravimetria_p3.ipynb`.

## Cultivos

### 1- (CC) Pruebas exploratorias en lab
- `alfalfa_01.txt` en lab, registrado voltaje y distancia con programa `src/arduino/cultivo/cultivo.ino`
- `nfmaiz_01.txt` en lab, registrado nf y distancia con programa `src/arduino/nf_cultivo/nf_cultivo.ino`