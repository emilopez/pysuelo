# Medición de variables agro-hidrológicas y calibración de sensores

## Organización del repositorio

En el repositorio se organizan y documentan los programas desarrollados, las pruebas realizadas, los datos obtenidos y su correspondiente análisis. Dentro de cada directorio un `README.md` explica en detalle su contenido.

- `src`: códigos fuente usados
  -  `arduino`: firmware para el dispositivo de medición
  -  `py`: scripts varios
-  `data`: archivos de datos registrados, dentro del directorio se explican los metadatos de cada archivo (prueba, lugar, firmware usado, etc).
- `img`: figuras y fotos varias
- `doc`: papers, hojas de datos, etc
- `proyecto_medición_humedad_suelo.md`: detalles del proyecto de medición de humedad de suelo
- `analisis_lab_distanciometros.ipynb`: visualización de datos registrados por los distanciómetros en lab
- `analisis_exploratorio_lab_gravimetria.ipynb`: análisis de prueba exploratoria con sensores capacitivos, sht10 al aire y fotos automáticas de la balanza

.. image:: https://mybinder.org/badge_logo.svg

 :target: https://mybinder.org/v2/gl/emilopez%2Fpysuelo/master?filepath=https%3A%2F%2Fgitlab.com%2Femilopez%2Fpysuelo%2Fblob%2Fmaster%2Fanalisis_exploratorio_sensores_capacitivos.ipynb

- `analisis_exploratorio_sensores_capacitivos.ipynb`: análisis del comportamiento de los sensores capacitivos al aire y sumergidos en agua.

## Diseño de Pruebas

Se realizan dos tipos de pruebas en simultáneo:
- Humedad de suelo (HS)
- Crecimiento de cultivos (CC)
  - Maíz y nivel freático (dispositivo A)
  - Alfalfa (dispositivo B)

## 5- (HS) Prueba en campo en simultáneo con sensores Hydraprobe2

- Fecha de inicio: 2020-06-16
- Programa arduino: ``src/arduino/combo_hum_suelo_fich/hp_mas_capacitivos/hp_mas_capacitivos.ino``
- Script python: ``src/py/pyescucha.py``

## 4- (HS) Prueba gravimétrica

Fecha de inicio: 2020-01-17

#### Elementos

- 2 muestras de un mismo suelo (arena)
- 2 Balanzas de precisión
- 11 sensores capacitivos (económicos): c0.. c10
- 2 SHT10 en (shtA y shtB)
- 3 DHT22 (dhtA, dhtB, dhtC) midiendo temperatura y humedad ambiente

#### Conexionado
- Todos los sensores soldados a la placa y luego encastrados al Arduino MEGA
- Fuente de alimentación externa 5v, 2A.


|                                          | Envase 1                           | Envase 2         |
|------------------------------------------|------------------------------------|------------------|
| Balanza                                  | Ohaus Scout Pro                    | Ohaus CS Serie   |
| Sensores capacitivos                     | v2.0                               | v1.2             |
| ID sensores                              | shtA, shtB, c0, c1, c2, c3, c4, c5 | c6, c7,c8,c9,c10 |
| Peso                                     | 128 gr                             | 128 gr           |
| Peso de arena                            | 2872 gr                            | 2872 gr          |
| Peso de agua                             |  500 gr                            |  500 gr          |
| Altura de arena                          |   9.4  cm                          | 9.4 cm           |
|  Peso Envase (E) + Tierra (T) + Agua (A) |   3500 gr                          |  3500 gr         |
|  Peso E + T + A + sensores               |   3676.6 gr                        |  3625 gr         |
| Timestamp incial                         |   2020-02-14 15:54                 | 2020-02-14 15:54 |

#### Códigos
- Arduino: ``src/arduino/gravimetria/gravimetria.ino``
- Python: corriendo en la raspi: ``pyescucha.py``

#### Resultados

- Datos: ``hum_suelo.csv`` y fotos
- Análisis: ``gravimetria_p4.ipynb``

Video a partir de las fotos: ``ffmpeg -pattern_type glob -i '*.jpeg' -vf format=yuv420p 2020-02-14_2020-04-05.mp4`` (usando https://trac.ffmpeg.org/wiki/Slideshow)


[![Gravimetria1](http://img.youtube.com/vi/Dlm4kR4mXh4/0.jpg)](https://youtu.be/Dlm4kR4mXh4 "Gravimetría del 2020-02-14 al 2020-04-05")

[![Gravimetria2](http://img.youtube.com/vi/8Ddt2xBfa2o/0.jpg)](https://youtu.be/8Ddt2xBfa2o "Gravimetría del 2020-04-05 al 2020-06-12")



## 3- (HS) Prueba gravimétrica

Fecha de inicio: 2020-01-17

Elementos

- 2 muestras de suelo independientes (1 y 2)
- 2 Balanzas de precisión
- 9 sensores capacitivos (económicos): c0.. c8
- 2 SHT10 en (shtA y shtB)
- 3 DHT22 (dhtA, dhtB, dhtC) midiendo temperatura y humedad ambiente

Procedimiento y conexionado
- Arduino MEGA conectado a todos los sensores, leyendo sus valores y escribíendolos en el puerto serie
- Raspberry Pi conectado por USB al Arduino,desde un script en python lée los valores cada 15 minutos, agrega un timestamp y los guarda en un archivo CSV y en simultáneo lo sube a internet (https://iot-sentinel.herokuapp.com/)
- Cada 15 minutos lanza un evento desde el `crontab` para tomar una fotografía y observar el display de las balanzas, almacenando las fotos con la fecha en su nombre de archivo.
- Posteriormente se deberá agregar una columna al archivo CSV con los valores de la balanza (probar OCR nuevamente o sino hacerlo manualmente)


Metadatos

|                                          | Envase 1                           | Envase 2         |
|------------------------------------------|------------------------------------|------------------|
| Balanza                                  | Ohaus Scout Pro                    | Ohaus CS Serie   |
| Sensores capacitivos                     | v2.0                               | v1.2             |
| ID sensores                              | shtA, shtB, c0, c1, c2, c3, c7, c8 | c4, c5, c6       |
| Peso                                     | 128 gr                             | 128 gr           |
| Peso de tierra                           | 1832 gr                            | 1940 gr          |
| % Agregado de agua                       | 40%                                | 35%              |
| Altura de tierra                         | 9.5 cm                             | 8.5 cm           |
|  Peso Envase (E) + Tierra (T) + Agua (A) | 2694.6 gr                          | 2746 gr          |
|  Peso E + T + A + sensores               | 2795.3 gr                          | 2739 gr          |
| Tiempo incial                            | 16:00 hs                           | 16:00 hs         |

- **Datos**: los datos se almacenarán en `data/p03_gravimetria.csv`
- **Mas fotos:** https://mega.nz/#F!ywVUSAgT!fXm6GPJckCqrhH6HmuAjTw

<img src="img/hs_exploratoria/20200117_163859.jpg" width="250" />
<img src="img/hs_exploratoria/20200117_153258.jpg" width="250" />
<img src="img/hs_exploratoria/20200117_210643.jpg" width="250" />
<img src="img/hs_exploratoria/20200117_153239.jpg" width="250" />
<img src="img/hs_exploratoria/20200117_152617.jpg" width="250" />
<img src="img/hs_exploratoria/20200114_180126.jpg" width="250" />
<img src="img/hs_exploratoria/20200117_152422.jpg" width="250" />
<img src="img/hs_exploratoria/20200114_180108.jpg" width="250" />


## 2- (HS) Prueba exploratoria sensores capacitivos al AIRE y en AGUA

- En laboratorio, registros de valores de sensores al aire y completamente sumergidos en agua.
- Se determinó el rango valores de cada sensor, en AIRE y AGUA. Estos valores luego mapean al porcentaje de humedad
- Al igual que en la prueba previa se observaron los saltos de los sensores debido al reinicio de Arduino ante la consulta serial. Se **solucionó** ejecutando el script python en segundo plano mediante `nohup python3 py_escucha_guarda.py &`
- Ahora el script `src/py/py_escucha_guarda.py` abre -y no cierra- el puerto serie, mientras que espera el período preconfigurado para leer por el puerto serie (mejorar)
- Se agregó al script para que suba todos los datos a `https://iot-sentinel.herokuapp.com/` (proyecto `https://gitlab.com/emilopez/iot`)

## 1- (HS) Prueba gravimetría exploratoria

- Prueba gravimétrica (fallida): automatización de fotos, registro de 2 sensores capacitivos y 2 SHT10 al aire.

```bash
*/30 * * * * fswebcam -d /dev/video0 -r 1280x960 --jpeg 55 -F 5 /home/pi/gravimetria/$(date +\%F-\%H_\%M).jpeg
*/30 * * * * /usr/bin/python3 /home/pi/py_escucha_guarda.py >> /home/pi/log_hum_suelo.log 2>&1
```

- Fluctuación de los sensores debido al reinicio del arduino en cada consulta por el puerto serie, esto se automatizaba desde `cron`
- Dinámica del agua en el suelo, primero evapora la parte superficial, luego la inferior florece, debido a esto probablemente los sensores dispuestos en forma vertical no del el mejor resultado.
- Intento de extracción del peso de la balanza mediante OCR `src/py/ocr.temp.py`, se debería acercar la cámara pero se perdería de vista el suelo. Lo dejo, se puede hacer manualmente en forma rápida.



