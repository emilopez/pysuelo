# Tipos de sensores de humedad de suelo

Constante dieléctrica del aire: 1 
Constante dieléctrica del agua: 80

La presencia de agua en el suelo genera una variación en su permitividad eléctrica, modificando la capacidad del capacitor suelo-sensor. La desventaja es la fuerte dependencia a la textura del suelo, por tanto se requiere una calibración específica para cada tipo de suelo.

Las técnicas dieléctricas se dividen en time-domain y frecuency-domain.

Time Domain Reflectometry (TDR) determina la permitividad eléctrica del suelo midiendo el tiempo de viaje para una señal de pulso electromagnético a lo largo de una guía de onda paralela.

En la técnica frecuency-domain dos principios fundamentales pueden encontrarse: Frequency Domain Reflectometry (FDR) y Capacitancia.

El primero engloba una serie de metodologías que utilizan una onda estacionaria de voltaje en el dominio de la frecuencia, como resultado de una señal incidente y reflejada. Dependiendo del parámetro seleccionado, algunos establecen relación entre permitividad y frecuencia de resonancia, mientras que otros relacionan la permitividad con el coeficiente de reflexión y transmisión.

Con respecto a las técnicas de capacitancia, la permitividad del medio se determina midiendo el tiempo de carga de un capacitor que modifica la frecuencia de operación de un oscilador. Este condensador consta de dos electrodos, manteniendo el suelo como dieléctrico entre ellos.

Los sensores HydraProbe ii utiliza un principio de medición de "impedancia dieléctrica" que difiere de los sensores de suelo TDR, capacitancia y frecuencia al tener en cuenta el almacenamiento de energía y la pérdida de energía en el área del suelo utilizando una onda de radiofrecuencia de 50 MHz.