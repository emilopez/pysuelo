#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

#define DL_CS 10
#define PowDL 9
#define MINUTOS 60

File myFile;

/*
 Registro: CRECIMIENTO CULTIVO ALFALFA en alfalfa.txt
    - timestamp
    - voltaje (A1)
    - 10 distancias (A2)
*/


void setup() {
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
}

void loop() {
    // despertar
    // =========
    Serial.println("prendiendo...");
    pinMode(PowDL, OUTPUT); 
    pinMode(DL_CS, OUTPUT);
    digitalWrite(PowDL,HIGH);
    delay(50);
    rtc.begin();
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    delay(80);
    if (!SD.begin(DL_CS)) {
        Serial.println("SD failed!");
    }else
        Serial.println("SD OK");
    
    // medir
    // =====
    delay(200);
    String ts = get_timestamp()+" ";
    float volt = analogRead(A1) * (5 / 1023.0);
    delay(3000);
    
    // guardar
    // =======
    Serial.println("guardando...");
    myFile = SD.open("test.txt", FILE_WRITE);
    delay(200);
    if (myFile) {
        myFile.print(ts);
        myFile.print(";");
        myFile.print(volt);
        myFile.print(";");
        Serial.print(ts);
        Serial.print(";");
        Serial.print(volt);
        Serial.print(";");
        for (byte i=0; i<29; i++){
            myFile.print(analogRead(A2));
            myFile.print(";");
            Serial.print(analogRead(A2));
            Serial.print(";");
            delay(100);
        }
        myFile.print(analogRead(A2));
        myFile.println();
        
    } else {
        Serial.println("error opening file");
    }
    myFile.close();

    // dormir
    // ======
    Serial.println("apaga");
    digitalWrite(PowDL,LOW);
    delay(10);
    // duerme por x minutos
    for (int c = 0; c < MINUTOS*60/8; c++) { 
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
    }
    delay(10);
    Serial.println("up");
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
