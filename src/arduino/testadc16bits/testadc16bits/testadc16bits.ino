#include <Wire.h>
#include <Adafruit_ADS1015.h> // ADC16bits, OJO 1 bit es para el signo, por tanto son 15 NOMAS!!!

Adafruit_ADS1115 ads; 

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    ads.setGain(GAIN_TWO); // OJO: 2.048V es el maximo valor que va a leer el MODULO
    ads.begin();
}

void loop() {
  // put your main code here, to run repeatedly:
    unsigned int adc0;

    adc0 = ads.readADC_SingleEnded(0);
    double v = 2.048*adc0/32768.0;
    Serial.println(v);
    delay(500);
}
