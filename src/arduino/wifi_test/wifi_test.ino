#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

#include "WiFiEsp.h"
 
// Emulate Serial1 on pins 6/7 if not present
#ifndef HAVE_HWSERIAL1
#include "SoftwareSerial.h"
SoftwareSerial serwifi(2, 3); // RX, TX 
#endif



//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/* =================================================
 Registro: eq3t2.txt
    - timestamp
    - voltaje (A1)
  ==================================================*/

#define DL_CS 10
#define PowDL 9


File myFile;
String get_timestamp();

void setup(){
    delay(1000);
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    Serial.println("setup");
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();

    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println(F("SD Ko"));
    }else
      Serial.println(F("SD OK"));

    
    serwifi.begin(115200);
    WiFi.init(&serwifi);
    // check for the presence of the shield
    if (WiFi.status() == WL_NO_SHIELD) {
      Serial.println("WiFi shield not present");
      // don't continue
      while (true);
    }
  
    // Print WiFi MAC address
    printMacAddress();
    
}

void loop(){
    delay(5000);
    digitalWrite(6,LOW);
    Serial.println("down");
    delay(5000);
    digitalWrite(6,HIGH);
    Serial.println("high");
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void printMacAddress(){
  // get your MAC address
  byte mac[6];
  WiFi.macAddress(mac);

  // print MAC address
  char buf[20];
  sprintf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", mac[5], mac[4], mac[3], mac[2], mac[1], mac[0]);
  Serial.print("MAC address: ");
  Serial.println(buf);
}
