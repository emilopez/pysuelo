# Codigo para Hydraprobe 2

Retorna los siguientes valores:

id sensor
H soil moisture
J soil coductivity (temp corrected)
F soil temp (celsius)

G soil temp (Farenheit)
O soil conductivity
K Real Dielectric Permittivity

M Imaginary Dielectric Permittivity
L Real Dielectric Permittivity (temp. corrected)
N Imaginary Dielectric Permittivity (temp. Corrected)
