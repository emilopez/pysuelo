#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"


// 0X3C+SA0 - 0x3C or 0x3D
#define I2C_ADDRESS 0x3C

// Define proper RST_PIN if required.
#define RST_PIN -1

SSD1306AsciiAvrI2c oled;


//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/* =========================
   Registro: eq5.txt
    - timestamp
    - voltaje (A1)
  ==========================*/

#define DL_CS 10
#define PowDL 9

uint16_t MINUTOS[4]={5, 15, 30, 60};
byte idx_min = 0;

File myFile;
String get_timestamp();

void setup(){
    delay(1000);
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    Serial.println("setup");
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println(F("bootE"));
    }else
      Serial.println(F("SD OK"));
    myFile = SD.open(F("eq5.txt"), FILE_WRITE);
    myFile.print("#");
    myFile.print(get_timestamp());
    myFile.println(" BOOT");
    myFile.close();

    #if RST_PIN >= 0
        oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    #else // RST_PIN >= 0
        oled.begin(&Adafruit128x64, I2C_ADDRESS);
    #endif // RST_PIN >= 0
    // Call oled.setI2cClock(frequency) to change from the default frequency.

    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.print(F("BootOK"));
}

void loop(){
    // despertar e inicializar
    // =======================
    unsigned long tiempo_inicio = millis();
    delay(150);
    Serial.println(F("on"));
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(1000);
    rtc.begin();
    delay(100);
    SD.begin(DL_CS);
    delay(100);
    
    #if RST_PIN >= 0
        oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    #else // RST_PIN >= 0
        oled.begin(&Adafruit128x64, I2C_ADDRESS);
    #endif // RST_PIN >= 0
    // Call oled.setI2cClock(frequency) to change from the default frequency.

    oled.setFont(System5x7);
    oled.setContrast(1);
    oled.clear();
  
    // MEDICION
    // ========
    String ts = get_timestamp();
    float volt = analogRead(A1) * (5 / 1023.0);

    
    Serial.print(ts);      Serial.print(F(";"));
    Serial.println(volt);
  
    oled.clear();
    oled.println(ts);
    oled.print(volt);    oled.println(F(" v"));
   
    
    // ALMACENA
    // =========
    myFile = SD.open(F("eq5.txt"), FILE_WRITE);
    delay(1000);
    if (myFile) {
        myFile.print(ts);
        myFile.print(";");
        myFile.println(volt);
        //set_sleeptime();
        
        oled.println(F("SD OK"));
    } else {
        oled.clear();
        Serial.println(F("IO"));
        oled.println(F("IO"));
    }
    myFile.close();
  
    // DORMIR
    // ======
    int sleep = rtc.now().minute() % MINUTOS[idx_min];
    if (sleep != 0){
        sleep = MINUTOS[idx_min] - sleep;
    } else {
        sleep = MINUTOS[idx_min];
    }
    //convierto a SEGUNDOS y resto segundos transcurridos
    sleep = sleep*60 - ((int)rtc.now().second());
    //apago
    digitalWrite(PowDL,LOW);
    delay(200);
    // tambien resto tiempo transcurrido hasta aca en segundos
    sleep = sleep - (millis()-tiempo_inicio)/1000;
    
    //Serial.println(sleep);
    delay(200);
    // duerme arduino de a 4 segundos
    for (int i = 0; i < (sleep/4); i++) { 
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF); 
    }
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void set_sleeptime(uint16_t dis){
    // cambio tiempo de sleep segun la distancia
    if (dis <= 100) 
        idx_min = 0;
    else if (dis <=200)
        idx_min = 1;
    else if (dis <=250)
         idx_min = 2;
    else
        idx_min = 3;
}
