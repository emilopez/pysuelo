#include <SD.h>
#include <SoftwareSPI2.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DHT.h>

#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

/*=======================
 EQUIPO 1: 
    - timestamp
    - voltaje (A6)
    - MONITOR A0
    - nivel freatico (5, 6, 7)
=========================*/

#define CS 5
#define SCK 6
#define MISO 7
#define DL_CS 10
#define PowDL 9
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define MONITORbtn A0
#define BATPIN A6
#define DHTPIN 8

String get_timestamp();
float transferFunction(uint16_t dataIn);

uint16_t      MINUTOS[4]={5, 15, 30, 60};
byte          IDX_MIN = 0;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot = true;


File myFile;
DHT dhtA(DHTPIN, DHTTYPE);
SoftwareSPI2 spi(SCK, MISO, CS);
SSD1306AsciiAvrI2c oled;



void setup(){
    delay(1000);
    spi.begin();
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(900);
    dhtA.begin();
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    if (!SD.begin(DL_CS)) {
      oled.println(F("SD Ko"));
    }else{
      myFile = SD.open("eq1.txt", FILE_WRITE);
      delay(1000);
      if (myFile) {
          myFile.println("ok");
          myFile.close();
          oled.println(F("SD OK"));
          oled.print(F("T="));
          oled.println(MINUTOS[IDX_MIN]);
      } else {
          oled.println(F("IO"));
      }
    }
    delay(3000);
}

void loop(){
    // DESPIERTA
    // =========
    unsigned long tiempo_inicio = millis();
    bool MONITOR = analogRead(MONITORbtn) > 1000;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();        
        SD.begin(DL_CS);
        delay(900);
        dhtA.begin();
        
        
        // MEDICION
        // =========
        
        //timestamp, voltaje, HR, temp, NF cmca
        String ts = get_timestamp();
        float volt = analogRead(BATPIN) * (5 / 1023.0);
        int hum_amb = dhtA.readHumidity();
        float tem_amb = dhtA.readTemperature();      
        spi.deselect();                     // NF: deselecciono y selecciono para activar
        delay(100);
        pinMode(CS, OUTPUT); 
        spi.select();
        byte firstByte, secondByte;
        uint16_t data;
        float cmca;

        //for (byte i = 0;i<5;i++){
            firstByte = spi.transfer(0x00);
            secondByte = spi.transfer(0x00);
            data = (firstByte << 8) | secondByte;
            cmca = transferFunction(data)*70.4;
            delay(200);
        //}
        
        // ALMACENA
        // ========
        if (wakeUp){
            char fname[13];
            String sfname = get_timestamp();
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
        
            delay(1000);
            if (myFile) {
                if (boot){
                    myFile.print("#EQ1-NF;");
                    boot = 0;
                }
                myFile.print(ts);
                myFile.print(";");
                myFile.print(volt);
                myFile.print(";");
                myFile.print(cmca);
                myFile.print(";");
                myFile.print(hum_amb);
                myFile.print(";");
                myFile.println(tem_amb);
            } else {
                oled.println(F("IO"));
            }
            
            myFile.close();
            
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;
        }
        if (MONITOR){
            // SALIDA PANTALLA
            // ===============
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.println(ts);  
            oled.println();
            
            oled.set2X();
            oled.print(cmca); 
            oled.print("cm");
            oled.set1X();
            oled.println();
            oled.println();
            
            oled.set2X();
            oled.print(hum_amb);  oled.print("% "); oled.print(tem_amb);oled.println("c"); 
            oled.print(analogRead(BATPIN) * (5 / 1023.0)); oled.print("v");
            delay(3000);
        }
    }else{
        // DORMIR
        // ======
        //desenergiza SD, RTC y SENSORES
        
        digitalWrite(PowDL,LOW);                       
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
