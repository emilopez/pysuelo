/*
 * Monitor que mapea 0 a 100 los sensores2 y 3
*/

const int AirValue_s2 = 620;   //you need to replace this value with Value_1
const int AirValue_s3 = 612;   //you need to replace this value with Value_1

const int WaterValue_s2 = 256;  //you need to replace this value with Value_2
const int WaterValue_s3 = 248;  //you need to replace this value with Value_2

int soilMoistureValue_s2   = 0;
int soilMoistureValue_s3   = 0;
int soilmoisturepercent_s2 = 0;
int soilmoisturepercent_s3 = 0;
int raw2=0;
int raw3=0;
void setup() {
  Serial.begin(9600); // open serial port, set the baud rate to 9600 bps
}
void loop() {
    //Serial.print(analogRead(A0));  //put Sensor insert into soil
    //Serial.print(",");
    //Serial.println(analogRead(A1));
    raw2 = analogRead(A0);
    raw3 = analogRead(A1);
    soilMoistureValue_s2 = raw2;
    soilMoistureValue_s3 = raw3;
    
    soilmoisturepercent_s2 = map(soilMoistureValue_s2, AirValue_s2, WaterValue_s2, 0, 100);
    soilmoisturepercent_s3 = map(soilMoistureValue_s3, AirValue_s3, WaterValue_s3, 0, 100);
    
    if(soilmoisturepercent_s2 > 100){
        soilmoisturepercent_s2 = 100;
    }else if(soilmoisturepercent_s2 <0){
        soilmoisturepercent_s2 = 0;
    }
    if(soilmoisturepercent_s3 > 100){
        soilmoisturepercent_s3 = 100;
    }else if(soilmoisturepercent_s3 <0){
        soilmoisturepercent_s3 = 0;
    }
    Serial.print(raw2);  //put Sensor insert into soil
    Serial.print(",");
    Serial.print(raw3);
    Serial.print(",");
    Serial.print(soilmoisturepercent_s2);  //put Sensor insert into soil
    Serial.print(",");
    Serial.println(soilmoisturepercent_s3);
    delay(250);
}
