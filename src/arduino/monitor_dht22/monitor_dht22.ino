#include <DHT.h>;

//Constants
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dhtA(2, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dhtB(3, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
DHT dhtC(4, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino

void setup(){
    Serial.begin(9600);
    dhtA.begin();
    dhtB.begin();
    dhtC.begin();
}

void loop(){
    Serial.print(dhtA.readHumidity());
    Serial.print(",");
    Serial.print(dhtB.readHumidity());
    Serial.print(",");
    Serial.print(dhtC.readHumidity());
    Serial.print(",");
    Serial.print(dhtA.readTemperature());
    Serial.print(",");
    Serial.print(dhtB.readTemperature());
    Serial.print(",");
    Serial.println(dhtC.readTemperature());

    
    delay(2000); //Delay 2 sec.
}
