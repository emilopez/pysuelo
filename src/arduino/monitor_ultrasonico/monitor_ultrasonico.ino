#include <LowPower.h>
/*
con esto pruebo el mb7092 con lectura analogica que ya tira en cm
y de dormirlo por 8 segundos que en modo rs232 no funcionaba

la prueba dio OK, duerme y mide bien.
la lectura del puerto analogica ya es en cm
*/

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
    //LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
    Serial.println(analogRead(A0));
    delay(100);
}
