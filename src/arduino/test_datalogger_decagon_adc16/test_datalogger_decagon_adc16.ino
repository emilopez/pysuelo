#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

#include <Wire.h>
#include <Adafruit_ADS1015.h> // ADC16bits, OJO 1 bit es para el signo, por tanto son 15 NOMAS!!!

/*=======================
 Descripcion:
    - MONITOR A6
    - BATERIA A7
=========================*/

#define DL_CS 10
#define PowDL 9
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define MONITORbtn A6
#define BATPIN A7

String get_timestamp();

uint16_t      MINUTOS[4]={1, 15, 30, 60};
byte          IDX_MIN = 1;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot = true;

Adafruit_ADS1115 ads; 
File myFile;

SSD1306AsciiAvrI2c oled;

void setup(){
    delay(1000);
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(900);
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    if (!SD.begin(DL_CS)) {
      oled.println(F(" SD error"));
    }else{
      oled.println(F(" SD OK"));
      oled.print(F(" T:"));
      oled.print(MINUTOS[IDX_MIN]);
      oled.print(F("min"));
    }
    delay(3000);
}

void loop(){
    Serial.println("loop");
    // DESPIERTA
    // =========
    unsigned long tiempo_inicio = millis();
    bool MONITOR = analogRead(MONITORbtn) > 1000;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();        
        SD.begin(DL_CS);
        delay(900);
        ads.setGain(GAIN_TWO); // OJO: 2.048V es el maximo valor que va a leer el MODULO
        ads.begin();

        // MEDICION
        // =========
        
        //timestamp, voltaje, decagon voltaje via ADC15bits
        String         ts = get_timestamp();
        float        volt = analogRead(BATPIN) * (5 / 1023.0);
        unsigned int adc0 = ads.readADC_SingleEnded(0);
        double   hum_volt = 2.048*adc0/32768.0;
 
        delay(50);
        
        // ALMACENA
        // ========
        if (wakeUp){
            //Serial.println("tocaba prender");
            char fname[13];
            String sfname = get_timestamp();
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
        
            delay(1000);
            if (myFile) {
                if (boot){
                    myFile.print("#EQ-decatest;");
                    boot = 0;
                }
                myFile.print(ts);
                myFile.print(";");
                myFile.print(volt);
                myFile.print(";");
                myFile.println(hum_volt);
            } else {
                oled.println(F("IO"));
            }
            
            myFile.close();
            
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;
        }
        if (MONITOR){
            Serial.println("muestra pantalla");
            // SALIDA PANTALLA
            // ===============
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.print(" ");
            oled.println(ts);
            oled.println();
            oled.print(" ");
            oled.set2X();
            oled.print(hum_volt); 
            oled.print("v");
            oled.set1X();
            oled.println();
            oled.println();
            oled.println();
            oled.print(" ");
            oled.set2X();
            oled.print(analogRead(BATPIN) * (5 / 1023.0)); oled.print("v");
            delay(3000);
        }
    }else{
        Serial.println("ini dormir");
        // DORMIR
        // ======
        //desenergiza SD, RTC y SENSORES
        digitalWrite(PowDL,LOW);                       
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
