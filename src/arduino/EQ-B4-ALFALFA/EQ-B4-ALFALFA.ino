#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DHT.h>

#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/*
 Registro: CRECIMIENTO Alfalfa en EQ-B4
    - timestamp
    - A0: distanciometro
    - A1: distanciometro
    - A6: distanciometro
    - MONITOR: Digital 3
    - DHT22  : Digital 5
    - A7: Voltaje 
*/

#define DHTTYPE DHT22     // DHT 22  (AM2302)
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define DL_CS 10
#define PowDL 9
#define MONITORbtn 3   
#define BATPIN A7
#define DIST1_PIN A0
#define DIST2_PIN A1
#define DIST3_PIN A6
#define DHTPIN 5

uint16_t      MINUTOS[4]={1, 15, 30, 60};
byte          IDX_MIN = 2;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot;

File myFile;
String get_timestamp();
DHT dhtA(DHTPIN, DHTTYPE);
SSD1306AsciiAvrI2c oled;
bool MONITOR;

void setup(){
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    pinMode(MONITORbtn, INPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    MONITOR = digitalRead(MONITORbtn)== HIGH;
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    rtc.begin();
    dhtA.begin();
    Serial.begin(9600);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    delay(100);
    if (!SD.begin(DL_CS)) {
      oled.println(F(" IOBoot"));
    }else
      oled.println(F(" SD OK"));
    if (MONITOR){
        rtc.setDateTime(DateTime(__DATE__, __TIME__));
        oled.println(" RTC set");
    }
    boot = true;
    delay(3000);
}

void loop(){
    unsigned long tiempo_inicio = millis();
    MONITOR = digitalRead(MONITORbtn) == HIGH;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();        
        SD.begin(DL_CS);
        delay(900);
        dhtA.begin();
    
        
        // MIDE Y ALMACENA 
        // ===============
        String ts = get_timestamp();
        float tem_amb = dhtA.readTemperature();
        byte hum_amb = dhtA.readHumidity();
        if (wakeUp){
            char fname[13];
            String sfname = get_timestamp();
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
            if (myFile){
                if (boot){
                    myFile.print(F("#EQ-B4")); 
                    boot = false;
                }
                // rafaga de 10 mediciones por cada sensor
                static const uint8_t cultivo[5] = {DIST1_PIN, DIST2_PIN, DIST3_PIN};   //A7
                for (byte parcela=0; parcela<3; parcela++){
                    myFile.print(ts); 
                    myFile.print(F(";"));
                    myFile.print(parcela+1); 
                    myFile.print(F(";"));
                    for (byte muestra=0; muestra<10; muestra++){
                        myFile.print(analogRead(cultivo[parcela]));
                        myFile.print(F(";"));
                    }
                    myFile.println();
                }
                myFile.print(ts);
                myFile.print(F(";ht;"));
                myFile.print(hum_amb);              
                myFile.print(F(";"));
                myFile.println(tem_amb);
                myFile.print(ts); 
                myFile.print(F(";"));
                myFile.print(F("v")); 
                myFile.print(F(";")); 
                myFile.println(analogRead(BATPIN) * (5 / 1023.0));    //a6
            } else {
                Serial.println(F("IO"));
            }
            myFile.close();
        
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;
        }
        if (MONITOR){
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.println(ts);  
            oled.println();
            oled.print(" P1:"); oled.print(analogRead(DIST1_PIN)); oled.print(" P2:"); oled.print(analogRead(DIST2_PIN));
            oled.print(" P3:"); oled.println(analogRead(DIST3_PIN)); 
            oled.println();
            oled.println();
            oled.print(hum_amb);  oled.print("%  "); oled.print(tem_amb);
            oled.print("C  "); oled.print(analogRead(BATPIN) * (5 / 1023.0)); oled.print("v");
            delay(3000);
        }
    }else {
        // DORMIR
        // ======
        digitalWrite(PowDL,LOW);                       // APAGA
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
