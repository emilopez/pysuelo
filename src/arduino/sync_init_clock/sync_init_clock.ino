#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

//rtc.setDateTime(DateTime(__DATE__, __TIME__));

/* README
 * ======

Test syncro al inicio> para que guarde cada X minutos en horas redondas

*/

#define DL_CS 10
#define PowDL 9

uint16_t MINUTOS[4]={5, 15, 30, 60};
byte idx_min = 0;

File myFile;
String get_timestamp();

void setup(){
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println("SDboot failed!");
    }else
      Serial.println("SD OK");
    myFile = SD.open("syncro.txt", FILE_WRITE);
    myFile.print("#");
    myFile.print(get_timestamp());
    myFile.println(" BOOT");
    myFile.close();
}

void loop(){
    // despertar
    // =========
    Serial.println("prendiendo...");
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    
    // inicializar
    // ===========
    
    rtc.begin();
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println("SD failed!");
    }//else
     // Serial.println("SD OK");
   

    // save data
    // =========
    myFile = SD.open("syncro.txt", FILE_WRITE);
    if (myFile){
        Serial.print(get_timestamp());
        Serial.print(";");
        Serial.println(analogRead(A1) * (5 / 1023.0));    
        
        myFile.print(get_timestamp()); 
        myFile.print(";");
        myFile.println(analogRead(A1) * (5 / 1023.0));    
    } else {
        Serial.println("error opening file");
    }
    
    myFile.close();
  
    // dormir x minutos
    // ================
    int sleep = rtc.now().minute() % MINUTOS[idx_min];
    if (sleep != 0){
        sleep = MINUTOS[idx_min] - sleep;
    } else {
        sleep = MINUTOS[idx_min];
    }
    Serial.println("apagando...");
    delay(100);
    digitalWrite(PowDL,LOW);
    Serial.println("durmiendo...");
    delay(10);
    // duerme por x minutos
    for (int i = 0; i < sleep*15; i++) { 
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF); 
    }
    delay(100);
}


String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void set_sleeptime(){
    // cambio tiempo de sleep segun la distancia
    if ((analogRead(A1) * (5 / 1023.0)) < 3.7)
        idx_min = 3;
    if ((analogRead(A1) * (5 / 1023.0)) > 4)
        idx_min = 0;
    
}
