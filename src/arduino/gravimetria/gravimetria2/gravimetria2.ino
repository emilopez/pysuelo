#include <SHT1x.h>
#include <DHT.h>


//Constants
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dhtA(5, DHTTYPE); 
DHT dhtB(6, DHTTYPE); 
DHT dhtC(7, DHTTYPE); 

// data and clock connections of SHT1x
#define dataPinA  9
#define dataPinB  10
#define clockPin 11
SHT1x shtA(dataPinA, clockPin);
SHT1x shtB(dataPinB, clockPin);

void setup(){
    dhtA.begin();
    dhtB.begin();
    dhtC.begin();
    Serial.begin(9600); // Open serial connection to report values to host
    //Serial.println("#hdhtA, hdhtB, hdhtC, tdhtA, tdhtB, tdhtC, tshtA, tshtB, hshtA, hshtB, 11cap...");
}

void loop(){
    float tempA = shtA.readTemperatureC();
    float humeA = shtA.readHumidity();

    float tempB = shtB.readTemperatureC();
    float humeB = shtB.readHumidity();
  
    // Print the values to the serial port
    Serial.print(dhtA.readHumidity());
    Serial.print(",");
    Serial.print(dhtB.readHumidity());
    Serial.print(",");
    Serial.print(dhtC.readHumidity());
    Serial.print(",");
    Serial.print(dhtA.readTemperature());
    Serial.print(",");
    Serial.print(dhtB.readTemperature());
    Serial.print(",");
    Serial.print(dhtC.readTemperature());
    Serial.print(",");
    Serial.print(tempA);
    Serial.print(",");
    Serial.print(tempB);
    Serial.print(",");
    Serial.print(humeA);
    Serial.print(",");
    Serial.print(humeB);
    Serial.print(",");
    Serial.print(analogRead(A0));
    Serial.print(",");
    Serial.print(analogRead(A1));
    Serial.print(",");
    Serial.print(analogRead(A2));
    Serial.print(",");
    Serial.print(analogRead(A3));
    Serial.print(",");
    Serial.print(analogRead(A4));
    Serial.print(",");
    Serial.print(analogRead(A5));
    Serial.print(",");
    Serial.print(analogRead(A6));
    Serial.print(",");
    Serial.print(analogRead(A7));
    Serial.print(",");
    Serial.println(analogRead(A8));
    Serial.print(",");
    Serial.println(analogRead(A9));
    Serial.print(",");
    Serial.println(analogRead(A10));
    
    //delay(2000);
}
