#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DHT.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
#include <SPI.h>

#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define DHTTYPE DHT22     // DHT 22  (AM2302)
//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/* =================================================
 Registro: yyyymmdd.txt> timestamp;voltaje;humedad;temperatura;precipitacion;anemometro
    
  ==================================================*/

#define DL_CS 10
#define PowDL 9
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define MONITORbtn A0
#define BATPIN A6
#define CANGILONmm 0.25
#define DHTPIN 4
#define ANEMOMETROPIN A2

SSD1306AsciiAvrI2c oled;
DHT dht(4, DHTTYPE); 

uint16_t          MINUTOS[4] = {5, 15, 30, 60};
volatile byte     IDX_MIN = 0;
volatile uint32_t ultimo_cangilon = 0;
volatile uint32_t cangilones_hoy = 0;
uint32_t          cangilones_ayer=0, cangilones_antes_ayer = 0;
bool              wakeUp = true;
uint32_t          count_sleep = 0;
int               sleep;
bool              boot;

byte    dia_hoy, dia_ayer;
String  ts;
float   volt;
int     hum_amb;
float   tem_amb;
float   viento;

File myFile;
String get_timestamp();

void setup(){
    //Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    pinMode(DHTPIN, INPUT); 
        
    digitalWrite(PowDL,HIGH);
    delay(100);
    // inicializacion
    rtc.begin();
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    delay(100);
    if (!SD.begin(DL_CS)) {
      oled.println(F("IOboot"));
    }else
      oled.println(F("SD OK"));
    oled.print(F("T="));
    oled.print(MINUTOS[IDX_MIN]);
    DateTime now = rtc.now();
    dia_hoy = (byte)now.date();
    boot = true;
    
    attachInterrupt(digitalPinToInterrupt(2), cangilon, FALLING); //RISING
    delay(3000);
}

void loop(){
    unsigned long tiempo_inicio = millis();   
    bool MONITOR = analogRead(MONITORbtn)>1000;
    
    // MONITOR
    // ======= 
    if (wakeUp || MONITOR){
        // despierta e inicializa
        // ----------------------
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();
        SD.begin(DL_CS);
        delay(900);
        dht.begin();

        // MEDICION
        // --------
        char sep=';';
        ts = get_timestamp();
        delay(2100);
        volt = analogRead(BATPIN) * (5 / 1023.0);
        hum_amb = dht.readHumidity();
        tem_amb = dht.readTemperature();
        viento = 0.004887586*analogRead(ANEMOMETROPIN); //0.004887586 = 5/1023.0
        if (viento <= 0.45)
            viento = 0;
        else
            viento = 20.25*viento - 8.1;
        if (MONITOR){
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.println(ts);
            oled.println();
            oled.set2X();
            oled.print("HOY ");  oled.println(CANGILONmm*cangilones_hoy); 
            oled.set1X();
            oled.print("AYER "); oled.println(CANGILONmm*cangilones_ayer); 
            oled.print("AAYER ");oled.println(CANGILONmm*cangilones_antes_ayer); 
            oled.print(viento); oled.println(" m/s");
            oled.print(hum_amb);  oled.print("%  "); oled.print(tem_amb); oled.print("C  "); oled.print(volt); oled.print("v");
            delay(2000);
        }

        if (wakeUp){
          
            // ALMACENA
            // =========
            char fname[13];
            String sfname = get_timestamp();
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
            delay(1000);
            if (myFile) {
                if (boot){
                    boot = false;
                    myFile.print(F("#EQ3t;v;h;t;p;a"));    
                }
                myFile.print(ts);
                myFile.print(sep);
                myFile.print(volt);
                myFile.print(sep);
                myFile.print(hum_amb);
                myFile.print(sep);
                myFile.print(tem_amb);
                myFile.print(sep);
                myFile.print(cangilones_hoy);
                myFile.print(sep);
                myFile.println(viento);
            } else {
                oled.println(F("IO"));
            }
            myFile.close();
            
            // pone a 0 si cambia dia
            // ----------------------
            DateTime now = rtc.now();
            if  (dia_hoy != (byte)now.date()){
                dia_hoy = (byte)now.date();
                cangilones_antes_ayer = cangilones_ayer;
                cangilones_ayer = cangilones_hoy;
                cangilones_hoy = 0;
            }
            
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            digitalWrite(PowDL,LOW);                       // APAGA 
            delay(100);
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;                                // a MiMiR
        }
    }else {
        // DORMIR
        // ======
        //desenergiza SD, RTC y SENSORES
        
        digitalWrite(PowDL,LOW);                       
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
    
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void cangilon(){
    if ((millis() - ultimo_cangilon) > 150 ){
        cangilones_hoy += 1;
        ultimo_cangilon = millis();
        //Serial.println(cangilones_hoy);
    }
}
