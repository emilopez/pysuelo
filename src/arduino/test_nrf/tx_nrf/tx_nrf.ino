//https://github.com/dparson55/NRFLite

#include <SPI.h>
#include <NRFLite.h>

NRFLite radio;
//uint8_t data;
char data[] = "hola mundo";

void setup(){
    radio.init(0, 7, 8); // Set transmitter radio to Id = 0, along with the CE and CSN pins
    Serial.begin(9600);
}

void loop(){
    //data++; // Change some data.
    radio.send(1, &data, sizeof(data)); // Send to the radio with Id = 1
    Serial.println(data);
    delay(1000);
}
