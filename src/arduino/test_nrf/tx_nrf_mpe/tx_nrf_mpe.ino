//PFC mpe
// envio 6 flotantes
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte pipe = 1;                      
 
float datos_env[6] = {1.12, 2.22, 3.13, 4.14, 5.13, 6.0};
byte payload[32];

void setup() {
    // Setup radio.
    radio.begin();
    radio.setPALevel(RF24_PA_LOW);
    radio.setDataRate(RF24_250KBPS);
    radio.openWritingPipe(pipe);
}

void loop() {  
    
    // Send data. directamente los flotantes
    radio.write(datos_env, sizeof(datos_env));
    
    // con esto puedo armar un payload de distintos tipos de dato, pero lo desacoplo en bytes, 
    // por ejemplo mando los 6 flotantes en un arreglo de bytes
    //memcpy((payload), (byte *)(datos_env), 4*6);
    //radio.write(payload, 24);
    
    delay(800);
}
