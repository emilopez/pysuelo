//https://github.com/bjarne-hansen/py-nrf24
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte tx_addr[6] = "00001";                      

byte protocol = 1;
byte payload[32] = "3 prueba"; 

void setup() {
    // Setup radio.
    radio.begin();
    radio.enableDynamicPayloads();
    radio.setAutoAck(true);
    //radio.setDataRate(RF24_1MBPS);
    radio.openWritingPipe(tx_addr);
    radio.stopListening();

}

void loop() {  

    // Send payload.
    radio.write(payload, sizeof(payload));
    delay(800);
}
