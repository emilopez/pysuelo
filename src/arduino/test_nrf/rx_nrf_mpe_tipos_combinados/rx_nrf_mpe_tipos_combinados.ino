
/* Recibe un arreglo de 14 bytes que se deben interpretar en 6 variables:
      - byte  = 1 byte
      - byte  = 1 byte
      - int   = 2 bytes
      - int   = 2 bytes
      - float = 4 bytes
      - float = 4 bytes
*/

#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte pipe = 1;                      

byte  env1;
byte  env2;
int   env3;
int   env4;
float env5;
float env6;

void setup() {
    Serial.begin(9600);
    // Setup radio.
    radio.begin();
    radio.setPALevel(RF24_PA_LOW);
    radio.setDataRate(RF24_250KBPS);
    radio.openReadingPipe(1,pipe);
    radio.startListening();
    Serial.println("init");
}

void loop() {  
    byte datos_rec[32];
    
    // recive data
    if(radio.available()){
        radio.read(datos_rec, 14);
        env1 = datos_rec[0];
        env2 = datos_rec[1];
        memcpy((&env3), (byte *)(datos_rec+2 ), 2);
        memcpy((&env4), (byte *)(datos_rec+4 ), 2);
        memcpy((&env5), (byte *)(datos_rec+6 ), 4);
        memcpy((&env6), (byte *)(datos_rec+10), 4);
        Serial.println();
        Serial.println("RECIBIDO");
        Serial.println("========");
        Serial.println(env1);
        Serial.println(env2);
        Serial.println(env3);
        Serial.println(env4);
        Serial.println(env5);
        Serial.println(env6);
    } else {
        Serial.print(".");
    }

    
    delay(100);
}
