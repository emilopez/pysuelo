//PFC mpe
// recibo 6 flotantes 

#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte pipe = 1;                      
int wait=0;

void setup() {
    Serial.begin(9600);
    // Setup radio.
    radio.begin();
    radio.setPALevel(RF24_PA_LOW);
    radio.setDataRate(RF24_250KBPS);
    radio.openReadingPipe(1,pipe);
    radio.startListening();
}

void loop() {  
    float datos_rec[6]={0,0,0,0,0,0};
    // recive data
    if(radio.available()){
        radio.read(datos_rec, sizeof(datos_rec));
        wait = 0;
    }
    if (wait == 0)
        for (byte i=0;i<6;i++)
            Serial.println(datos_rec[i]);
    Serial.println(++wait);
    
    Serial.println();
    delay(800);
}
