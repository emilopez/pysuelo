// envio y guarda en SD
/* Envia un arreglo de 14 bytes que se deben interpretar en 6 variables:
      - byte  = 1 byte
      - byte  = 1 byte
      - int   = 2 bytes
      - int   = 2 bytes
      - float = 4 bytes
      - float = 4 bytes
*/
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>
#include <SD.h> //SD card
#include <SPI.h>
#include <Wire.h>

#define SDCSPin 10 // pin 10 is CS pin for MicroSD breakout
#define POWDL 9
#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte pipe = 1;

byte payload[32];
int count=1;

void setup() {
    Serial.begin(9600);
    SPI.begin(); //spi
    //Wire.begin(); //i2c

    pinMode(SDCSPin, OUTPUT);    //
    pinMode(POWDL, OUTPUT);
    digitalWrite(POWDL,HIGH);
    delay(520);


    if (!SD.begin(SDCSPin)) {
      Serial.println(F("IOboot"));
    }else
      Serial.println(F("SD OK"));


}

void loop() {
    // con esto puedo armar un payload de distintos tipos de dato, pero lo desacoplo en bytes,
    // por ejemplo mando los 6 flotantes en un arreglo de bytes
    char fname[13];
    String sfname = "20201215.CSV";
    sfname.toCharArray(fname,15);

    SD.begin(SDCSPin);
    delay(500);
    File myfile = SD.open(fname, FILE_WRITE);
    delay(500);
    if (myfile) {
        count++;
        myfile.println(count);
        myfile.close();
        Serial.println("saved");
    }else{
        Serial.println("error");
    }
    //digitalWrite(SDCSPin,HIGH);
    delay(100);
    int      y = 2020;
    byte  env1 = (byte)(y%100);
    byte  env2 = 12;
    int   env3 = 888;
    int   env4 = 999;
    float env5 = 3.14;
    float env6 = 1.41;

    // armo el payload
    payload[0] = env1;
    payload[1] = env2;
    memcpy((payload+2 ), (byte *)(&env3), 2);
    memcpy((payload+4 ), (byte *)(&env4), 2);
    memcpy((payload+6 ), (byte *)(&env5), 4);
    memcpy((payload+10), (byte *)(&env6), 4);

    radio.begin();
    radio.setPALevel(RF24_PA_LOW);
    radio.setDataRate(RF24_250KBPS);
    radio.openWritingPipe(pipe);
    delay(100);
    radio.write(payload, 14);

    delay(500);
}
