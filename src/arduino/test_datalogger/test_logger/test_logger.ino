#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

/*=======================
 EQUIPO 1: 
    - timestamp
    - voltaje (A1)
    - nivel freatico (5, 6, 7)
=========================*/

#define CS 5
#define SCK 6
#define MISO 7
#define DL_CS 10
#define PowDL 9
#define DHTTYPE DHT22   // DHT 22  (AM2302)
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define MONITORbtn A0
#define BATPIN A6

String get_timestamp();

uint16_t      MINUTOS[4]={3, 15, 30, 60};
byte          IDX_MIN = 0;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot = true;


File myFile;
SSD1306AsciiAvrI2c oled;

void setup(){
    delay(1000);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(900);
    
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    if (!SD.begin(DL_CS)) {
      oled.println(F("SD Ko"));
    }else{
      myFile = SD.open("logtest.txt", FILE_WRITE);
      delay(1000);
      if (myFile) {
          myFile.println("ok");
          myFile.close();
          oled.println(F("SD OK"));
      } else {
          oled.println(F("IO"));
      }
    }
    delay(3000);
}

void loop(){
    // DESPIERTA
    // =========
    unsigned long tiempo_inicio = millis();
    if (wakeUp){
        // DESPERTAR E INICIALIZAR
        // =======================
        
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();        
        SD.begin(DL_CS);
        delay(900);
        
        
        // MEDICION
        // =========
        
        //timestamp, voltaje, HR, temp, NF cmca
        String ts = get_timestamp();
        float volt = 3.187*analogRead(BATPIN)*(5.0/1023.0);

        delay(100);
        pinMode(CS, OUTPUT); 
        
        // ALMACENA
        // ========
        
        char fname[13];
        String sfname = get_timestamp();
        sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
        sfname.toCharArray(fname,15);
        myFile = SD.open(fname, FILE_WRITE);
    
        delay(1000);
        if (myFile) {
            if (boot){
                myFile.print("#");
                boot = 0;
            }
            myFile.print(ts);
            myFile.print(";");
            myFile.println(volt);
        } else {
            oled.println(F("IO"));
        }
        
        myFile.close();
        
        // calcula tiempos que debe dormir
        // -------------------------------
        sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
        if (sleep != 0){
            sleep = MINUTOS[IDX_MIN] - sleep;
        } else {
            sleep = MINUTOS[IDX_MIN];
        }
        sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
        sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
        wakeUp = false;
        
    }else{
        // DORMIR
        // ======
        //desenergiza SD, RTC y SENSORES
        
        digitalWrite(PowDL,LOW);                       
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
