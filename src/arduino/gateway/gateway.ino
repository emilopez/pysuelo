
/* // NODO GATEWAY
 *        NRF EQ )) --> (( NRF GATEWAY --- EHTERHET ---> INTERNET
 *  
 *  Recibe un arreglo de 20 bytes que se deben interpretar en 11 variables:
 *                                                | id VARIABLE API
      - anio              = 1 byte                |
      - mes               = 1 byte                |
      - dia               = 1 byte                |
      - hora              = 1 byte                |
      - minuto            = 1 byte                |
      - segundo           = 1 byte                |
      - voltaje           = 4 bytes (float)       | 13
      - humedad amb       = 1 bytes               | 11
      - temperatura amb   = 4 bytes (float)       | 12
      - cangilones        = 2 bytes (int)         | 14
      - viento            = 4 bytes (float)       | 15
*/

#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>
#include <Ethernet.h>
#define PIN_CS  8
#define PIN_CE  7

RF24 radio(PIN_CE, PIN_CS);
const byte pipe = 1;                      

byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};  // Direccion MAC
IPAddress ip(192,168,100,111);                      // IP del arduino
IPAddress dns(192, 168, 100, 1);
char server[] = "www.agro-iot.duckdns.org";             // Direccion IP del servidor 000webhost.com
EthernetClient client;


byte  yy, mm, dd, hh, minu, seg, hume;
float voltaje, temp, viento;
int cangilones;

void setup() {
    Serial.begin(9600);
    // Setup radio.
    radio.begin();
    radio.setPALevel(RF24_PA_LOW);
    radio.setDataRate(RF24_250KBPS);
    radio.openReadingPipe(1,pipe);
    radio.startListening();
    Serial.println("init");
}

void loop() {  
    byte datos_rec[32];
    
    // recive data
    if(radio.available()){
        radio.read(datos_rec, 20);
        yy = datos_rec[0];
        mm = datos_rec[1];
        dd = datos_rec[2];
        hh = datos_rec[3];
        minu = datos_rec[4];
        seg =  datos_rec[5];
        memcpy((&voltaje   ), (byte *)(datos_rec+6 ), 4);
        memcpy((&hume      ), (byte *)(datos_rec+10), 1);
        memcpy((&temp      ), (byte *)(datos_rec+11), 4);
        memcpy((&cangilones), (byte *)(datos_rec+15), 2);
        memcpy((&viento    ), (byte *)(datos_rec+17), 4);
        Serial.println("=====");
        Serial.println(yy);
        Serial.println(mm);
        Serial.println(dd);
        Serial.println(hh);
        Serial.println(minu);
        Serial.println(seg);
        Serial.println(voltaje);
        Serial.println(hume);
        Serial.println(cangilones);
        Serial.println(viento);

        if (client.connect(server, 80)) {
            //Serial.print("connected to ");
            //Serial.println(client.remoteIP());
            // HTTP HEADER
            // ===========
            client.println("POST /api/datos/ HTTP/1.1");
            client.println("Authorization: Token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e");
            client.println("User-Agent: Arduino/1.0");
            client.println("Host: www.agro-iot.duckdns.org");
            client.println("Content-Type: application/json;");
            client.println("Connection: close");
            String mes = String(mm);
            if (mm<10)
                mes = "0" + String(mm);

            String dia = String(dd);
            if (dd<10)    
                dia = "0" + String(dd)+"T";

            String hora = String(hh);
            if (hh<10)
                hora = "0" + String(hh);

            String mminu = String(minu);
            if (minu<10)
                mminu = "0" + String(minu);

            String sseg = String(seg);
            if (seg<10)
                sseg = "0" + String(seg);

            String timestamp = "\"" + String(2000+yy) + "-" mes + "-" + dia + hora + ":" + mminu + ":" + sseg + "\"";
            
            // PAYLOAD
            // =======
            //String params = "[{\"variable\":10,\"timestamp\":\"2020-12-05T18:00:00\",\"valor\":3.14}]";
            String comilla = "\"";
            
            String params = "[{\"variable\":10,\"timestamp\":" + timestamp +",\"valor\":3.14}]";

            
            client.print("Content-Length: "); client.println(params.length());
            client.println();
            client.print(params);
        
        } else {
            // if you didn't get a connection to the server:
            Serial.println("connection failed");
        }
        delay(3000);
    
        while (client.available()){
            Serial.write(client.read());
            //delay(50);
        }
        Serial.println();
        Serial.println("--------------");
        
        if (client.connected())
            client.stop(); // st
        }

    
        delay(500);
}
