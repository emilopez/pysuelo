#include <DallasTemperature.h>
#include <OneWire.h> //Se importan las librerías
#define Pin2 2 //Se declara el pin donde se conectará la DATA
#define Pin3 3

OneWire ourWire1(Pin2); //Se establece el pin declarado como bus para la comunicación OneWire
OneWire ourWire2(Pin3); //Se establece el pin declarado como bus para la comunicación OneWire

DallasTemperature sensor1(&ourWire1); //Se instancia la librería DallasTemperature
DallasTemperature sensor2(&ourWire2); //

void setup(){
    Serial.begin(9600); // Open serial connection to report values to host
    sensor1.begin();
    sensor2.begin();
    Serial.println("#boot");
}

void loop(){
    sensor1.requestTemperatures();
    float t1 = sensor1.getTempCByIndex(0);
    
    sensor2.requestTemperatures();
    float t2 = sensor2.getTempCByIndex(0);
    Serial.print(t1);                Serial.print(",");
    Serial.print(t2);                Serial.print(",");
    Serial.print(analogRead(A0));    Serial.print(",");
    Serial.print(analogRead(A2));    Serial.print(",");
    Serial.print(analogRead(A4));    Serial.print(",");
    Serial.print(analogRead(A6));    Serial.print(",");
    Serial.print(analogRead(A8));    Serial.print(",");
    Serial.println(analogRead(A10));
    
    delay(2000);
}
