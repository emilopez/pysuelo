/*
 * 
 *  Tenemos dos instancias, sdiA y sdiB, la clave es que antes de usar cada uno hay que hacer
 *  el .begin(), eso lo activa y desactiva el resto. No puede haber mas de uno activo a la vez
 *  por eso es que no funciona usar los dos begin en el setup como usualmente hacemos
 * 
 * Luego, como siempre enviar "aM!" y luego "aD0!", "aD1!" y "aD2!"
*/

#include <SDI12.h>
#include <DHT.h>

//Constants
#define DHTTYPE DHT22   // DHT 22  (AM2302)
DHT dhtA(5, DHTTYPE); 
DHT dhtB(6, DHTTYPE); 
DHT dhtC(7, DHTTYPE); 

// en sdiA tengo 3 sensores con addr: 1, 2, 3
// en sdiB tengo 1 sensor con addr: 4
SDI12 sdiA(A15); 
SDI12 sdiB(A14); 
                          
void setup(){
    Serial.begin(9600); 
    dhtA.begin();
    dhtB.begin();
    dhtC.begin();
}

void loop(){
     String myCommand;
    // vamor a ir leyendo para los 3 sensores que estan con el cable de datos (AZUL) al pin 9
    //-------
    // sdiA
    //-------
    sdiA.begin();
    for (int addr = 1; addr<=3; addr++){
        // le avisamos que le vamos a pedir todo
        myCommand = String(addr) + "M!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        String sdiResponse="";
        // concatenamos la respuesta que es un nro por ej 30029, 3 es la direccion va a demorar 2 segundos
        // y nos devuelve 9 variables
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        // vaciamos el buffer
        sdiA.flush();
        delay(1000);
        // ahora leemos D0
        //----------------
        myCommand = String(addr) + "D0!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        // ahora leemos D1
        //----------------
        myCommand = String(addr) + "D1!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        // ahora leemos D2
        //----------------
        myCommand = String(addr) + "D2!";
        sdiA.sendCommand(myCommand); 
        delay(300);                     // wait a while for a response
        sdiResponse+=";";
        while(sdiA.available()){    // write the response to the screen
            char c = sdiA.read();
            if ((c != '\n') && (c != '\r')){
                sdiResponse += c;
                delay(5);
            }
        }
        sdiA.flush();
        delay(1000); 
        Serial.print("sensor h" + String(addr)+";");
        Serial.println(sdiResponse);
    }
    //-------
    // sdiB
    //-------
    sdiB.begin();
    // le avisamos que le vamos a pedir todo
    myCommand = "4M!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    String sdiResponse="";
    // concatenamos la respuesta que es un nro por ej 30029, 3 es la direccion va a demorar 2 segundos
    // y nos devuelve 9 variables
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    // vaciamos el buffer
    sdiB.flush();
    delay(1000);
    // ahora leemos D0
    //----------------
    myCommand = "4D0!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    // ahora leemos D1
    //----------------
    myCommand = "4D1!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    // ahora leemos D2
    //----------------
    myCommand = "4D2!";
    sdiB.sendCommand(myCommand); 
    delay(300);                     // wait a while for a response
    sdiResponse+=";";
    while(sdiB.available()){    // write the response to the screen
        char c = sdiB.read();
        if ((c != '\n') && (c != '\r')){
            sdiResponse += c;
            delay(5);
        }
    }
    sdiB.flush();
    delay(1000); 
    Serial.print("sensor h4;");
    Serial.println(sdiResponse);
    //----------------------------
    // ahora todos los analogicos
    //----------------------------
    Serial.print("sensor c0;");
    Serial.println(analogRead(A0));
    Serial.print("sensor c1;");
    Serial.println(analogRead(A1));
    Serial.print("sensor c2;");
    Serial.println(analogRead(A2));
    Serial.print("sensor c3;");
    Serial.println(analogRead(A3));
    Serial.print("sensor c4;");
    Serial.println(analogRead(A4));
    Serial.print("sensor c5;");
    Serial.println(analogRead(A5));
    Serial.print("sensor c6;");
    Serial.println(analogRead(A6));
    Serial.print("sensor c7;");
    Serial.println(analogRead(A7));
    Serial.print("sensor c8;");
    Serial.println(analogRead(A8));
    Serial.print("sensor c9;");
    Serial.println(analogRead(A9));
    Serial.print("sensor c10;");
    Serial.println(analogRead(A10));
    Serial.print("sensor dA;");
    Serial.print(dhtA.readHumidity());
    Serial.print(";");
    Serial.println(dhtA.readTemperature());
    Serial.print("sensor dB;");
    Serial.print(dhtB.readHumidity());
    Serial.print(";");
    Serial.println(dhtB.readTemperature());
    Serial.print("sensor dC;");
    Serial.print(dhtC.readHumidity());
    Serial.print(";");
    Serial.println(dhtC.readTemperature());

}
