#include <Ethernet.h>

byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};  // Direccion MAC
IPAddress ip(192,168,100,111);                      // IP del arduino
IPAddress dns(192, 168, 100, 1);
char server[] = "www.agro-iot.duckdns.org";             // Direccion IP del servidor 000webhost.com
EthernetClient client;

String gethttpHeader = "GET /api/datos/?page=last&variable=10 HTTP/1.1\nHost: agro-iot.duckdns.org\nAuthorization: Token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e\nUser-Agent: Arduino/1.0\nContent-Type: application/json;\nConnection: close\n";                 

/*
String posthttpHeader = "POST /api/datos/?.txt HTTP/1.1\n" +
                    "Host: agro-iot.duckdns.org\n" +
                    "Authorization: token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e\n" +
                    "User-Agent: Arduino/1.0\n" + 
                    "Connection: close\n" + 
                    "Content-Type: application/json;\n";                    
                    //"Content-Type: application/json; charset=UTF-8\n"
*/

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    Ethernet.begin(mac, ip, dns);
}

void loop() {
    delay(2000);
    // put your main code here, to run repeatedly:
    Serial.println("init");
    // if you get a connection, report back via serial:
    if (client.connect(server, 80)) {
        Serial.print("connected to ");
        Serial.println(client.remoteIP());
        // Make a HTTP request:
        //client.print(gethttpHeader);
        client.println("GET /api/datos/?page=last&variable=10 HTTP/1.1");
        client.println("Authorization: Token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e");
        client.println("User-Agent: Arduino/1.0");
        client.println("Host: www.agro-iot.duckdns.org");
        client.println("Content-Type: application/json;");
        client.println("Connection: close");
        client.println();
        // test google OK
        //client.println("GET /search?q=arduino HTTP/1.1");
        //client.println("Host: www.google.com");
        //client.println("Connection: close");
        //client.println();
    } else {
        // if you didn't get a connection to the server:
        Serial.println("connection failed");
    }
    delay(3000);
    
    while (client.available()){
        Serial.write(client.read());
        delay(50);
    }
    if (client.connected())
        client.stop(); // st
}
