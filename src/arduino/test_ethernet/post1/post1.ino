#include <Ethernet.h>

byte mac[] = {0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02};  // Direccion MAC
IPAddress ip(192,168,100,111);                      // IP del arduino
IPAddress dns(192, 168, 100, 1);
char server[] = "www.agro-iot.duckdns.org";             // Direccion IP del servidor 000webhost.com
EthernetClient client;

void setup() {
  // put your setup code here, to run once:
    Serial.begin(9600);
    Ethernet.begin(mac, ip, dns);
    
}

void loop() {
    delay(2000);    
    if (client.connect(server, 80)) {
        //Serial.print("connected to ");
        //Serial.println(client.remoteIP());
        // HTTP HEADER
        // ===========
        client.println("POST /api/datos/ HTTP/1.1");
        client.println("Authorization: Token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e");
        client.println("User-Agent: Arduino/1.0");
        client.println("Host: www.agro-iot.duckdns.org");
        client.println("Content-Type: application/json;");
        client.println("Connection: close");
        // PAYLOAD
        // =======
        String params = "[{\"variable\":10,\"timestamp\":\"2020-12-05T18:00:00\",\"valor\":3.14}]";
        client.print("Content-Length: "); client.println(params.length());
        client.println();
        client.print(params);
        
    } else {
        // if you didn't get a connection to the server:
        Serial.println("connection failed");
    }
    delay(3000);
    
    while (client.available()){
        Serial.write(client.read());
        //delay(50);
    }
    Serial.println();
    Serial.println("--------------");
    
    if (client.connected())
        client.stop(); // st
}
