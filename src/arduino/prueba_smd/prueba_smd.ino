#include <SD.h>
#include <SoftwareSPI2.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>

#define CS 5
#define SCK 6
#define MISO 7
#define DL_CS 10
#define PowDL 9

//SoftwareSPI2 spi(SCK, MISO, CS);

File myFile;
String get_timestamp();

void setup(){
    delay(1000);
    //spi.begin();
    Serial.begin(9600);
    Serial.println("boot");
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    Serial.println("setup");
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    //rtc.setDateTime(DateTime(__DATE__, __TIME__));
    if (!SD.begin(DL_CS)) {
      Serial.println("SDboot failed!");
    }else
      Serial.println("SDboot OK");
    myFile = SD.open("eq3.txt", FILE_WRITE);
    myFile.print("#");
    myFile.print(get_timestamp());
    myFile.println(" BOOT");
    myFile.close();
}

void loop(){
    // despertar
    // =========
    Serial.println("prendiendo...");
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println("SD failed!");
    }else
      Serial.println("SD OK");
    delay(100);
    //-------------
  
    String ts = get_timestamp();
    float volt = analogRead(A1) * (5 / 1023.0);
    Serial.print(ts);Serial.print(";");
    Serial.print(volt);Serial.print(";");
    
    // save data
    // =========
    myFile = SD.open("eq3.txt", FILE_WRITE);
    delay(1000);
    if (myFile) {
        myFile.print(ts);
        myFile.print(";");
        myFile.println(volt);
    } else {
        Serial.println("error opening file");
    }
    
    myFile.close();
    delay(8000);
  
    // dormir x minutos
    // ================
    Serial.println("apagando...");
    delay(100);
    digitalWrite(PowDL,LOW);
    Serial.println("durmiendo...");
    delay(10);
    // duerme por 5 minutos
    //for (int i = 0; i < (MINUTOS[idx_min]*60)/8; i++) { 
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
    //}
    delay(10);
    Serial.println("despertando...");
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}
