#include <SD.h>
#include <SoftwareSPI2.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DallasTemperature.h>
#include <OneWire.h> //Se importan las librerías
#define Pin 3 //Se declara el pin donde se conectará la DATA

//rtc.setDateTime(DateTime(__DATE__, __TIME__));
/*
 Registro: CRECIMIENTO CULTIVO MAIZ en nf_cultivo.txt
    - timestamp
    - nivel freatico
    - 10 distancias (A0)

*/

#define CS 5
#define SCK 6
#define MISO 7
#define DL_CS 10
#define PowDL 9

SoftwareSPI2 spi(SCK, MISO, CS);
OneWire ourWire(Pin); //Se establece el pin declarado como bus para la comunicación OneWire
DallasTemperature temp(&ourWire); //Se instancia la librería DallasTemperature


uint16_t MINUTOS[4]={1, 15, 30, 60};
byte idx_min = 2;

File myFile;
String get_timestamp();

void setup(){
    delay(1000);
    spi.begin();
    Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    Serial.println("setup");
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println("SD failed!");
    }else
      Serial.println("SD OK");
    myFile = SD.open("nfmaiz.txt", FILE_WRITE);
    myFile.print("#");
    myFile.print(get_timestamp());
    myFile.println(" BOOT");
    myFile.close();
}

void loop(){
    // despertar
    // =========
    Serial.println("prendiendo...");
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(100);
    if (!SD.begin(DL_CS)) {
      Serial.println("SD failed!");
    }else
      Serial.println("SD OK");
    temp.begin();
    delay(100);
    temp.requestTemperatures();
    //-------------
  
    // medir
    // =====
    
    spi.select();
    byte firstByte = spi.transfer(0x00);
    byte secondByte = spi.transfer(0x00);
    uint16_t data = (firstByte << 8) | secondByte;
    spi.deselect();
    //convertido a centimeros
    float cmca = transferFunction(data)*70.4;
    String ts = get_timestamp();
    float volt = analogRead(A1) * (5 / 1023.0);
    
    Serial.print(ts);   Serial.print(";");
    Serial.print(volt); Serial.print(";");
    Serial.println(cmca);
    
    //Serial.print(temp.getTempCByIndex(0));
    
    // save data
    // =========
    myFile = SD.open("nfmaiz.txt", FILE_WRITE);
    delay(1000);
    if (myFile) {
        myFile.print(ts);
        myFile.print(";");
        myFile.print(volt);
        myFile.print(";");
        myFile.print(temp.getTempCByIndex(0));
        //myFile.print(cmca);
        for (byte i=0; i<5; i++){
            myFile.print(";");
            myFile.print(analogRead(A0));
            Serial.print(";");
            Serial.print(analogRead(A0));
            delay(10);
        }
        set_sleeptime(analogRead(A0));
        myFile.println();
        Serial.println();
    } else {
        Serial.println("error opening file");
    }
    
    myFile.close();
  
    // dormir x minutos
    // ================
    Serial.println("apagando...");
    delay(100);
    digitalWrite(PowDL,LOW);
    Serial.println("durmiendo...");
    delay(10);
    // duerme por 5 minutos
    for (int i = 0; i < (MINUTOS[idx_min]*60)/8; i++) { 
        LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF); 
    }
    delay(10);
    Serial.println("despertando...");
  
}

float transferFunction(uint16_t dataIn) {
    float outputMax = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure
    float pressure = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}

void set_sleeptime(uint16_t dis){
    // cambio tiempo de sleep segun la distancia
    if (dis <= 100) 
        idx_min = 0;
    else if (dis <=200)
        idx_min = 1;
    else if (dis <=250)
         idx_min = 2;
    else
        idx_min = 3;
}
