#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include <DHT.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"

#include <SoftwareSPI2.h>
#include <nRF24L01.h>
#include <RF24_config.h>
#include <RF24.h>

//rtc.setDateTime(DateTime(__DATE__, __TIME__));

#define DHTTYPE DHT22     // DHT 22  (AM2302)
#define I2C_ADDRESS 0x3C  // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN -1        // Define proper RST_PIN if required.
#define DL_CS 10
#define PowDL 9
#define ECHOpin 2
#define TRIGGERpin 3
#define MONITORbtn A6
#define BATPIN A7
#define DHTPIN 5
#define AJUSTEVOLTAJEBATERIA 4.395   //5

#define CS_RF  8
#define CE_RF  7
RF24 radio(CE_RF, CS_RF);
const byte KEY[6] ="FICH2";
byte payload[32]={0};
int cont=0;

uint16_t      MINUTOS[4]={1, 15, 30, 60};
byte          IDX_MIN = 0;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot;

File myFile;
String get_timestamp();
DHT dhtA(DHTPIN, DHTTYPE);
SSD1306AsciiAvrI2c oled;
bool MONITOR;
float distancia;
float volt;

void setup(){
 
      
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT); 
    pinMode(MONITORbtn, INPUT); 
    pinMode(TRIGGERpin, OUTPUT);
    pinMode(ECHOpin, INPUT);
    digitalWrite(PowDL,HIGH);
    delay(100);
    MONITOR = analogRead(MONITORbtn);
// if (MONITOR>1000)
//       set_datetime();
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    rtc.begin();
    dhtA.begin();
    Serial.begin(9600);
   //-----------------------------
    radio.begin();
    radio.openWritingPipe(KEY);
    //radio.setPALevel(RF24_PA_LOW);   //    
    radio.setPALevel(RF24_PA_MAX);    // Potencia ajustada al máximo
    radio.setDataRate(RF24_250KBPS);
    radio.stopListening();
    delay(520);
   //-----------------------------
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set2X();
    delay(100);
    if (!SD.begin(DL_CS)) {
      oled.println(F(" IOBoot"));
    }else
 {     oled.println(F("SD OK"));
         oled.println();
      oled.print(F("T="));
      oled.print(MINUTOS[IDX_MIN]);
 }
    if (MONITOR){
         set_datetime();
         //rtc.setDateTime(DateTime(__DATE__, __TIME__));
        //oled.println(" RTC set");
    }
    boot = true;
    delay(3000);
}

void loop(){
    unsigned long tiempo_inicio = millis();
    MONITOR = analogRead(MONITORbtn)>1000;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT); 
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();        
        SD.begin(DL_CS);
        delay(900);
        dhtA.begin();
    
        
        // MIDE Y ALMACENA 
        // ===============
        String ts = get_timestamp();
        float tem_amb = dhtA.readTemperature();
        byte hum_amb = dhtA.readHumidity();
        volt= analogRead(BATPIN) * (AJUSTEVOLTAJEBATERIA / 1023.0);
        distancia = GetDistancia();
        if (wakeUp){
            char fname[13];
            String sfname = get_timestamp();
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
            if (myFile){
                if (boot){
                    myFile.print(F("#EQ-B12")); 
                    boot = false;
                }
                
          //---------------transmite-------------------------------------
            unsigned int packRTC[2];  // valor rtc
            cont++;
            ComprimeTimeStamp(packRTC);
            memcpy((payload+0), (byte *)(&cont), 4);
            memcpy((payload+4), (byte *)(packRTC), 4);
            memcpy((payload+8), (byte *)(&volt), 4);
            memcpy((payload+12), (byte *)(&distancia), 4);
            delay(500);
            
            radio.begin();
            radio.openWritingPipe(KEY);
            radio.setPALevel(RF24_PA_LOW);   //   radio.setPALevel(RF24_PA_LOW);    // Potencia ajustada al máximo
            radio.setDataRate(RF24_250KBPS);
            radio.stopListening();
            radio.write(payload, 16);  
            for(int i = 0;i<16;i++)
                Serial.print(payload[i],HEX);
            delay(500);
            Serial.println();
            
            //-------------------------
          
                
                myFile.print(ts); 
                myFile.print(F(";"));
                myFile.print(volt);    
                myFile.print(F(";ht;"));
                myFile.print(hum_amb);              
                myFile.print(F(";"));
                myFile.print(tem_amb);
                myFile.print(F(";"));
                myFile.print(distancia);
                myFile.println(F(";"));
              } else {
                Serial.println(F("IO"));
            }
            myFile.close();
        
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;
        }
        if (MONITOR){ 
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.println(ts);  
            oled.println();
            oled.set2X();
            Serial.println(distancia);
            //oled.println();
            oled.print("Dist:"); oled.println(distancia);
            oled.print(volt); oled.print("v");
            oled.println();
            oled.set1X();
            oled.println();
            oled.print(hum_amb);  oled.print("%  "); oled.print(tem_amb); oled.print("C  "); 
              delay(3000);
        }
    }else {
        // DORMIR
        // ======
        digitalWrite(PowDL,LOW);                       // APAGA
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

void set_datetime(){
    char eti[]={'A','M', 'D', 'h', 'm', 's','w'};
    byte val_max[] = {50,12,30,24,60,60,6};
    byte indice = 0, va1=-1,vn1,modificado=0;
    long tiempom; 
    char va=' ',vn;
    oled.clear(); 
    oled.set2X();
    oled.print("\n SET FECHA\n   HORA");
    delay(1000);
    while(analogRead(MONITORbtn)>1000);   //espera a que se suelte el boton para comenzar a setear
    DateTime now = rtc.now(); 
    byte valor[] = {now.year(),now.month(),now.date(),now.hour(),now.minute(),now.second(),now.dayOfWeek()}; 
    tiempom = millis();
    while (indice<6){
        if ((millis()-tiempom)>=2500){   //avance automatico
            ++indice;
            tiempom = millis();  
        }
        if (analogRead(MONITORbtn)>1000){    //entro cuando se presiona el boton
            modificado=1;
            if (indice==0 || indice>2 ){   //año, hora, minutos, segundos : 0 .. n
                  valor[indice] = (++valor[indice])%val_max[indice];
             }
            else if(indice==1){   //  meses : 1..12 
                    ++valor[indice];
                    if(valor[indice]>val_max[indice]) valor[indice]=1; 
                 } else if(indice==2){ 
                            if (valor[1]==2){
                                ++valor[indice];
                                if(valor[indice] > 28+(valor[0]%4==0?1:0)) valor[indice]=1;
                            }else{  
                                ++valor[indice];
                                if(valor[indice]> val_max[indice]+((valor[1]<=7 && valor[1]%2!=0)||(valor[1]>7 && valor[1]%2==0))? 1 : 0) valor[indice]=1;
              }
            }
            delay(200);  //velocidad de repeticion
            tiempom = millis(); 
        }
        vn = eti[indice];
        vn1 = valor[indice];
        if (va!=vn || va1!=vn1){
            oled.clear(); 
            oled.print(eti[indice]);
            oled.print(": ");
            if(indice==0) oled.print(valor[indice]+2000);
            else oled.print(valor[indice]);  
            va = vn; va1=vn1;
        }
    }
    if(modificado==1){
        DateTime dt(valor[0],valor[1], valor[2],valor[3],valor[4],valor[5], 5);
        rtc.setDateTime(dt);
    }
    oled.set1X();
    delay(1000);oled.clear();
}

float GetDistancia(){
    long duracion;
    float TEMPERATURA=dhtA.readTemperature();
    TEMPERATURA=clamp(TEMPERATURA,0.0f,50.0f); 
    float VelocidadSonido= 331.1 +(0.606 * TEMPERATURA);  
    digitalWrite(TRIGGERpin, LOW);
    delayMicroseconds(5);
    digitalWrite(TRIGGERpin, HIGH);
    delayMicroseconds(20);   // en este sensor recomiendan emitir por 20 microsegundos
    digitalWrite(TRIGGERpin, LOW);
    //Read the echoPin. pulseIn() returns the duration (length of the pulse) in microseconds:
    duracion = pulseIn(ECHOpin, HIGH);
    return (duracion*(VelocidadSonido/10000)/2);    //obtengo la distancia  (duracion*0.034/2)
}


int clamp(int valor, int mi, int ma){
  if(valor<mi) return mi;
  if(valor>ma) return ma;
  return valor;
}

float clamp(float valor, float mi, float ma){
  if(valor<mi) return mi;
  if(valor>ma) return ma;
  return valor;
}

 float transferFunction(uint16_t dataIn) {
    float outputMax   = 14746.0; // 2 to the 14th power (from sensor's data sheet)
    float outputMin   = 1638.0;
    float pressureMax = 15.0; // max 30 psi (from sensor's datea sheet)
    float pressureMin = -15.0;
    // transfer function: using sensor output to solve for pressure datasheet
    float pressure    = pressureMin + (dataIn - outputMin) * (pressureMax - pressureMin) / (outputMax - outputMin);
    return pressure;
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());   
    return t;
}


void ComprimeTimeStamp(unsigned int x[]){ //x[0]: dddddaaaaaMMMMMM dia año-2000 minutos      x[1]: 0mmmmhhhhhSSSSSS   mes hora segundos
    DateTime now = rtc.now();
    x[0] = now.date();
    x[0] = (x[0] << 5) | (now.year()-2000);               
    x[0] = (x[0] << 6) | now.minute();
    x[1] = now.month();
    x[1] = (x[1] << 5) | now.hour();
    x[1] = (x[1] << 6) | now.second();
}

String UnPackTimeStamp(unsigned long DT){                //  DT: 0dddddaaaaaMMMMMMmmmmHHHHHSSSSSS
      byte dia   = (DT >> 26) & 0B0000000000011111;                     
      byte anio  = (DT >> 21) & 0B0000000000011111;                     
      byte minu  = (DT >> 15) & 0B0000000000111111;                     
      byte mes   = (DT >> 11) & 0B0000000000001111;                      
      byte hora  = (DT >> 6)  & 0B0000000000011111;                      
      byte seg   =  DT        & 0B0000000000111111;                     
      String t = String(anio+2000) + "-";                     
      t += (mes < 10)? "0" + String(mes)   + "-": String(mes)  + " ";                     
      t += (dia < 10)? "0" + String(dia)   + " ": String(dia)   + " ";                      
      t += (hora< 10)? "0" + String(hora)  + ":": String(hora)   + ":";                     
      t += (minu< 10)? "0" + String(minu)  + ":": String(minu) + ":";                     
      t += (seg < 10)? "0" + String(seg)  : String(seg);                        
      return t;                     
}

unsigned long PackTimeStamp(){                //  DT: 0dddddaaaaaMMMMMMmmmmHHHHHSSSSSS    dia-año-minutos-mes-hora-segundos
    DateTime now = rtc.now();
    unsigned long DT = now.date();
    DT = (DT << 5) | (now.year()-2000);               
    DT = (DT << 6) | now.minute();
    DT = (DT << 4) | now.month();
    DT = (DT << 5) | now.hour();
    DT = (DT << 6) | now.second();
    return DT;

}
  

