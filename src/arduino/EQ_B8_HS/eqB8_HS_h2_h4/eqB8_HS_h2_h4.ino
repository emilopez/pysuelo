/* =================================================
   EQUIPO B8:  HUMEDAD DE SUELO
    - BASE: datalogger, display OLED, 
    
    * A6 boton MONITOR (A6)
    * A7 voltaje (A7); 
    * D5 HydraProbe (h4)  DATOS CABLE VERDE
    * D6 HydraProbe (h2)  DATOS CABLE AZUL
*/

#include <SD.h>
#include "Sodaq_DS3231.h"
#include <LowPower.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiAvrI2c.h"
#include <SDI12.h>

#define I2C_ADDRESS   0x3C     // 0X3C+SA0 - 0x3C or 0x3D
#define RST_PIN         -1     // Define proper RST_PIN if required.
#define DL_CS           10     // SD 
#define PowDL            9     // ON/OFF transistores
#define MONITORbtn      A6     // pulsador
#define BATPIN          A7     // voltage bateria
#define HYDRAPINSensor4  5     // Hydraprobe DATA PIN (sensor 4)
#define HYDRAPINSensor2  6     // Hydraprobe DATA PIN (sensor 2)

SDI12 hydra4(HYDRAPINSensor4);
SDI12 hydra2(HYDRAPINSensor2);
SSD1306AsciiAvrI2c oled;
File myFile;

uint16_t      MINUTOS[4] = {1, 15, 30, 60};
volatile byte IDX_MIN = 0;
bool          wakeUp = true;
uint32_t      count_sleep = 0;
int           sleep;
bool          boot, MONITOR = false;

unsigned long start, finished, elapsed;
void setup(){
    //Serial.begin(9600);
    pinMode(DL_CS, OUTPUT);
    pinMode(PowDL, OUTPUT);
    
    digitalWrite(PowDL,HIGH);
    delay(100);
    rtc.begin();
    delay(900);
    MONITOR = analogRead(MONITORbtn)>1000;
    oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
    oled.setFont(System5x7);
    oled.setContrast(255);
    oled.set1X();
    if (!SD.begin(DL_CS)) {
        oled.println(F(" SD error"));
        //Serial.println("SD error");
    }else
        oled.println(F(" SD OK"));
        //Serial.println("SD ok");
    oled.print(F(" T="));
    oled.println(MINUTOS[IDX_MIN]);
    boot = true;
    if (MONITOR){
        rtc.setDateTime(DateTime(__DATE__, __TIME__));
        oled.println(" RTC set");
    }
    delay(3000);
}

void loop(){
    //Serial.println("loop1");
    unsigned long tiempo_inicio = millis();
    MONITOR = analogRead(MONITORbtn)>1000;
    //wakeUp = true;
    if (wakeUp || MONITOR){
        // DESPERTAR E INICIALIZAR
        // =======================
        pinMode(DL_CS, OUTPUT);
        pinMode(PowDL, OUTPUT);
        digitalWrite(PowDL,HIGH);
        delay(100);
        rtc.begin();
        SD.begin(DL_CS);
        delay(900);
        hydra2.begin();

        // MEDICION
        // ========
        String ts = get_timestamp();
        char sep=';';
//==> sensor hydraprobe 2
        hydra2.sendCommand(F("2M!"));
        delay(300);                     // wait a while for a response
        while(hydra2.available()){
            hydra2.read();
            delay(5);
        }
        delay(1000);
        char cmd[3][5];
        strcpy(cmd[0], "2D0!");
        strcpy(cmd[1], "2D1!");
        strcpy(cmd[2], "2D2!");
        String hydraResponse2="";

        for (byte s=0; s<3; s++){
            hydra2.sendCommand(cmd[s]);
            delay(200);                     // wait a while for a response
            while(hydra2.available()){       // write the response to the screen
                char c = hydra2.read();
                if ((c != '\n') && (c != '\r')){
                    if (c == '+')
                        hydraResponse2 += sep;
                    else if (c == '-'){
                        hydraResponse2 += sep;
                        hydraResponse2 += '-';
                    }else
                        hydraResponse2 += c;
                    delay(5);
                }
            }
            delay(500);
        }
//==> sensor hydraprobe 4        
        hydra4.begin();
        hydra4.sendCommand(F("4M!"));
        delay(300);                     // wait a while for a response
        while(hydra4.available()){
            hydra4.read();
            delay(5);
        }
        delay(1000);
        //char cmd4[3][5];
        strcpy(cmd[0], "4D0!");
        strcpy(cmd[1], "4D1!");
        strcpy(cmd[2], "4D2!");
        String hydraResponse4="";

        for (byte s=0; s<3; s++){
            hydra4.sendCommand(cmd[s]);
            delay(200);                     // wait a while for a response
            while(hydra4.available()){       // write the response to the screen
                char c = hydra4.read();
                if ((c != '\n') && (c != '\r')){
                    if (c == '+')
                        hydraResponse4 += sep;
                    else if (c == '-'){
                        hydraResponse4 += sep;
                        hydraResponse4 += '-';
                    }else
                        hydraResponse4 += c;
                    delay(5);
                }
            }
            delay(500);
        }
        // ALMACENA
        // =========
        if (wakeUp){
            char fname[13];
            String sfname = ts;
            sfname = sfname.substring(0,4) + sfname.substring(5,7) + sfname.substring(8,10) + ".CSV";
            sfname.toCharArray(fname,15);
            myFile = SD.open(fname, FILE_WRITE);
            delay(200);
            if (myFile) {
                if (boot){
                    boot = false;
                    myFile.print(F("#HS_EQB8;"));
                }
                // voltage
                myFile.print(ts);
                myFile.print(sep);
                myFile.print("v;");
                myFile.println(analogRead(BATPIN) * (5 / 1023.0));
                
                // hydraprobe h2
                myFile.print(ts);
                myFile.print(sep);
                myFile.print("h2;");
                myFile.println(hydraResponse2);
                
                // hydraprobe h4
                myFile.print(ts);
                myFile.print(sep);
                myFile.print("h4;");
                myFile.println(hydraResponse4);
                
                //Serial.println("saveOK");
            } else {
                //Serial.println(F("IO"));
                oled.println(F("IO"));
            }
            myFile.close();
            // calcula tiempos que debe dormir
            // -------------------------------
            sleep = rtc.now().minute() % MINUTOS[IDX_MIN];
            if (sleep != 0){
                sleep = MINUTOS[IDX_MIN] - sleep;
            } else {
                sleep = MINUTOS[IDX_MIN];
            }
            sleep = sleep*60 - ((int)rtc.now().second());  //convierto a SEGUNDOS y resto segundos transcurridos
            sleep = sleep - (millis()-tiempo_inicio)/1000; // resto tiempo transcurrido hasta aca en segundos
            wakeUp = false;                                // a MiMiR
        }
        if (MONITOR){
            oled.begin(&Adafruit128x64, I2C_ADDRESS, RST_PIN);
            oled.setFont(System5x7);
            oled.setContrast(255);
            oled.clear();
            oled.set1X();
            oled.print(" ");
            oled.println(ts);
     // === hydraprobe h2
            oled.println("# Sensor H2");
            oled.print(" Hs:");
            byte p=3;
            while (hydraResponse2[p] != ';'){
                oled.print(hydraResponse2[p]);
                p++;
            }
            oled.println();
            p++;
            while (hydraResponse2[p] != ';') p++;
            p++;
            oled.print(" Ts:");
            while (hydraResponse2[p] != ';'){
                oled.print(hydraResponse2[p]);
                p++;
            }
            oled.println();
            oled.println("# Sensor H4");
    // === hydraprobe h4
            oled.print(" Hs:");
            p = 3;
            while (hydraResponse4[p] != ';'){
                oled.print(hydraResponse4[p]);
                p++;
            }
            oled.println();
            p++;
            while (hydraResponse4[p] != ';') p++;
            p++;
            oled.print(" Ts:");
            while (hydraResponse4[p] != ';'){
                oled.print(hydraResponse4[p]);
                p++;
            }
            
            oled.println();
            oled.println();
            oled.print(" ");
            oled.print(analogRead(BATPIN)*(5.0/1023.0)); oled.print("v");
            delay(3000);
        }

    }else{
        // DORMIR
        // ======
        digitalWrite(PowDL,LOW);                       // APAGA
        delay(100);
        count_sleep += 1;
        if (count_sleep >= sleep/4){
            wakeUp = true;
            count_sleep = 0;
        }
        LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);
        delay(100);
    }
}

String get_timestamp(){
    DateTime now = rtc.now();
    String t = String(now.year()) + "-";
    t += (now.month()  < 10)? "0" + String(now.month() )   + "-": String(now.month())  + "-";
    t += (now.date()   < 10)? "0" + String(now.date()  )   + " ": String(now.date())   + " ";
    t += (now.hour()   < 10)? "0" + String(now.hour()  )   + ":": String(now.hour())   + ":";
    t += (now.minute() < 10)? "0" + String(now.minute())   + ":": String(now.minute()) + ":";
    t += (now.second() < 10)? "0" + String(now.second())        : String(now.second());

    return t;
}
