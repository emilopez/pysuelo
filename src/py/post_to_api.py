import requests
from datetime import datetime
port = 80
headers = {'Authorization': 'Token e86b816f29b2d232e96bd2ff8f8d0461ee03b40e', 'content-type': 'application/json'}
url = f"http://agro-iot.duckdns.org:{port}/api/datos/"
ts  = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
# c0 -> variable 2
data = [{'variable':10, 'timestamp':ts, 'valor':'3.33'}]

r = requests.post(url, headers=headers, json=data)
print(r.text)
