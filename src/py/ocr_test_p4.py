# ocr para prueba 4
# import the necessary packages
from imutils.perspective import four_point_transform
from imutils import contours
import imutils
import cv2
import glob
# define the dictionary of digit segments so we can identify
# each digit on the thermostat
DIGITS_LOOKUP = {
	(1, 1, 1, 0, 1, 1, 1): 0,
	(0, 0, 1, 0, 0, 1, 0): 1,
	(1, 0, 1, 1, 1, 1, 0): 2,
	(1, 0, 1, 1, 0, 1, 1): 3,
	(0, 1, 1, 1, 0, 1, 0): 4,
	(1, 1, 0, 1, 0, 1, 1): 5,
	(1, 1, 0, 1, 1, 1, 1): 6,
	(1, 0, 1, 0, 0, 1, 0): 7,
	(1, 1, 1, 1, 1, 1, 1): 8,
	(1, 1, 1, 1, 0, 1, 1): 9,
	(1, 0, 1, 0, 1, 0, 1): 1,
}

dire = "src/py/data/img_p4/"
images = glob.glob(f"{dire}/*.jpeg")
# load the example image
for img in images:
	image = cv2.imread(img)
	print(img, end=" ")
	dx,dy = 0,0
	display1 = image[742-dy:790+dy,335-dx:465+dx]
	display2 = image[745-dy:780+dy,868-dx:941+dx]
	cv2.imwrite(f"{img}-b1.jpeg", display1)
	cv2.imwrite(f"{img}-b2.jpeg", display2)

	#display1 = image[725:805,282:525]
	#display2 = image[745:780,868:941]

	rows,cols,_ = display1.shape

	M = cv2.getRotationMatrix2D((cols/2,rows/2),2.2,1)
	dst1 = cv2.warpAffine(display1, M, (cols,rows))
	#cv2.imshow('test', dst1)
	#cv2.waitKey(0)

	rows,cols,_ = display2.shape

	M = cv2.getRotationMatrix2D((cols/2,rows/2),2,1)
	dst2 = cv2.warpAffine(display2, M, (cols,rows))
	#cv2.imshow('test', dst2)
	#cv2.waitKey(0)

	# pre-process the image by resizing it, converting it to
	# graycale, blurring it, and computing an edge map  
	#image = imutils.resize(display1, height=500)
	gray = cv2.cvtColor(dst1, cv2.COLOR_BGR2GRAY)
	#blurred = cv2.GaussianBlur(gray, (3, 3), 0)
	#edged = cv2.Canny(blurred, 50, 200, 255)

	# threshold the warped image, then apply a series of morphological
	# operations to cleanup the thresholded image
	thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_TRIANGLE)[1]
	kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (1, 5))
	thresh = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
	#cv2.imshow('test', thresh)
	#cv2.waitKey(0)

	# find contours in the thresholded image, then initialize the
	# digit contours lists
	cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
	cnts = imutils.grab_contours(cnts)
	digitCnts = []
	# loop over the digit area candidates

	for c in cnts:
		# compute the bounding box of the contour
		(x, y, w, h) = cv2.boundingRect(c)
		# if the contour is sufficiently large, it must be a digit
		if (w >= 5) and (h >= 30):
			digitCnts.append(c)
			cv2.rectangle(dst1, (x, y), (x+w, y+h), (255,0,0), 2)
	#print("/////")
	#print(digitCnts)
	
	#cv2.imshow('test', dst1)
	#cv2.waitKey(0)


	digitCnts = contours.sort_contours(digitCnts, method="left-to-right")[0]
	digits = []

	# loop over each of the digits
	for c in digitCnts:
		# extract the digit ROI
		(x, y, w, h) = cv2.boundingRect(c)
		
		roi = thresh[y:y + h, x:x + w]
		# compute the width and height of each of the 7 segments
		# we are going to examine
		(roiH, roiW) = roi.shape
		(dW, dH) = (int(roiW * 0.25), int(roiH * 0.15))
		dHC = int(roiH * 0.05)
		# define the set of 7 segments
		segments = [
			((0, 0), (w, dH)),	# top
			((0, 0), (dW, h // 2)),	# top-left
			((w - dW, 0), (w, h // 2)),	# top-right
			((0, (h // 2) - dHC) , (w, (h // 2) + dHC)), # center
			((0, h // 2), (dW, h)),	# bottom-left
			((w - dW, h // 2), (w, h)),	# bottom-right
			((0, h - dH), (w, h))	# bottom
		]
		on = [0] * len(segments)

		# loop over the segments
		for (i, ((xA, yA), (xB, yB))) in enumerate(segments):
			# extract the segment ROI, count the total number of
			# thresholded pixels in the segment, and then compute
			# the area of the segment
			segROI = roi[yA:yB, xA:xB]
			total = cv2.countNonZero(segROI)
			area = (xB - xA) * (yB - yA)
			# if the total number of non-zero pixels is greater than
			# 50% of the area, mark the segment as "on"
			if total / float(area) > 0.4:
				on[i]= 1
		# lookup the digit and draw it on the image
		digit = DIGITS_LOOKUP.get(tuple(on), on)
		digits.append(digit)
		#cv2.rectangle(output, (x, y), (x + w, y + h), (0, 255, 0), 1)
		#cv2.putText(output, str(digit), (x - 10, y - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
	print(digits)	
	#print(u"{}{}.{} \u00b0C".format(*digits))
	#cv2.imshow("Input", image)
	#cv2.imshow("Output", output)
	#cv2.waitKey(0)