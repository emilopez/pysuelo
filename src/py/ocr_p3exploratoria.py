import os
import glob
import pytesseract
import pyocr
import pyocr.builders
import gpyocr
from PIL import Image


tools = pyocr.get_available_tools()
tool = tools[0]
'''
dire = "/home/emiliano/data/dev/proyectos_git/pysuelo/src/py/data"
pics = glob.glob(f"{dire}/*.jpeg")
pics.sort()

display_box = 599, 651, 739, 703
'''

print("filename, pyt, pyo, gpy")
pic             = '/home/emiliano/data/dev/proyectos_git/pysuelo/src/py/data/2020-01-17-16_00.jpeg'
display_izq_box = 251, 763, 427, 833
img             = Image.open(pic)
img_display     = img.crop(box=display_izq_box)
gray            = img_display.convert('L')
blackwhite      = gray.point(lambda x: 0 if x < 150 else 255, '1')

gray.save(f"{img.filename[-21:-5]}_gray.jpg")
blackwhite.save(f"{img.filename[-21:-5]}_bw.jpg")

#img_display.save(f"{img.filename[-21:-5]}.jpg")
digits_pyt = pytesseract.image_to_string(blackwhite, lang= "fra", config="digits")
digits_pyo = tool.image_to_string(blackwhite, lang="fra", builder=pyocr.tesseract.DigitBuilder())
digits_gpy = gpyocr.tesseract_ocr(blackwhite, lang='fra', psm=12)[0]

print(img.filename[-21:-5], digits_pyt, digits_pyo, digits_pyo, sep=",", end = "")

