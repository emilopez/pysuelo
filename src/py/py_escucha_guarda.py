import serial
import sys
import requests

from time import sleep
from datetime import datetime

headers = {'Authorization': 'Token daf7ebbbaf084ea1ee493b6ace103013286f8e74', 'content-type': 'application/json'}
url = 'https://iot-sentinel.herokuapp.com/api/datos/'

baud = 9600
baseports = ['/dev/ttyUSB', '/dev/ttyACM', 'COM', '/dev/tty.usbmodem1234']
ser = None

while not ser:
    for baseport in baseports:
        if ser:
            break
        for i in range(0, 64):
            try:
                port = baseport + str(i)
                ser = serial.Serial(port, baud, timeout=1)
                print("Monitor: Opened " + port + '\r')
                break
            except:
                ser = None
                pass

id_to_variable_nro = {
    "hume_dhtA":17,
    "hume_dhtB":18,
    "hume_dhtC":19,
    "temp_dhtA":15,
    "temp_dhtB":16,
    "temp_dhtC":20,
    "temp_shtA":14,
    "temp_shtB":13,
    "hume_shtA":11,
    "hume_shtB":12,
    "c0":2,
    "c1":3,
    "c2":4,
    "c3":5,
    "c4":6,
    "c5":7,
    "c6":8,
    "c7":9,
    "c8":10,
}


while True:
    minutos = int(datetime.now().strftime('%M'))
    if minutos % 5 == 0:
        for i in range(10):
            f = open("hum_suelo.txt", "a")
            timestamp = str(datetime.now().strftime('%Y-%m-%d %H:%M:%S')) 
            val = ser.readline().decode('ascii')
            print(timestamp + ',', val)
            f.write(timestamp + ',')
            f.write(val)
            f.close()
            datos = val.split(',')
            if len(datos) == 19:
                hume_dhtA,hume_dhtB,hume_dhtC,temp_dhtA,temp_dhtB,temp_dhtC,temp_shtA,temp_shtB,hume_shtA,hume_shtB,c2,c3,c0,c1,c4,c5,c7,c8,c6 = datos
            
                data = [{'timestamp':timestamp,
                     'valor':hume_dhtA,
                     'variable':id_to_variable_nro['hume_dhtA']
                    },
                    {'timestamp':timestamp,
                     'valor':hume_dhtB,
                     'variable':id_to_variable_nro['hume_dhtB']
                    },
                    {'timestamp':timestamp,
                     'valor':hume_dhtC,
                     'variable':id_to_variable_nro['hume_dhtC']
                    },
                    {'timestamp':timestamp,
                     'valor':temp_dhtA,
                     'variable':id_to_variable_nro['temp_dhtA']
                    },
                    {'timestamp':timestamp,
                     'valor':temp_dhtB,
                     'variable':id_to_variable_nro['temp_dhtB']
                    },
                    {'timestamp':timestamp,
                     'valor':temp_dhtC,
                     'variable':id_to_variable_nro['temp_dhtC']
                    },
                    {'timestamp':timestamp,
                     'valor':temp_shtA,
                     'variable':id_to_variable_nro['temp_shtA']
                    },
                    {'timestamp':timestamp,
                     'valor':temp_shtB,
                     'variable':id_to_variable_nro['temp_shtB']
                    },
                    {'timestamp':timestamp,
                     'valor':hume_shtA,
                     'variable':id_to_variable_nro['hume_shtA']
                    },
                    {'timestamp':timestamp,
                     'valor':hume_shtB,
                     'variable':id_to_variable_nro['hume_shtB']
                    },
                    {'timestamp':timestamp,
                     'valor':c0,
                     'variable':id_to_variable_nro['c0']
                    },
                    {'timestamp':timestamp,
                     'valor':c1,
                     'variable':id_to_variable_nro['c1']
                    },
                    {'timestamp':timestamp,
                     'valor':c2,
                     'variable':id_to_variable_nro['c2']
                    },
                    {'timestamp':timestamp,
                     'valor':c3,
                     'variable':id_to_variable_nro['c3']
                    },
                    {'timestamp':timestamp,
                     'valor':c4,
                     'variable':id_to_variable_nro['c4']
                    },
                    {'timestamp':timestamp,
                     'valor':c5,
                     'variable':id_to_variable_nro['c5']
                    },
                    {'timestamp':timestamp,
                     'valor':c6,
                     'variable':id_to_variable_nro['c6']
                    },
                    {'timestamp':timestamp,
                     'valor':c7,
                     'variable':id_to_variable_nro['c7']
                    },
                    {'timestamp':timestamp,
                     'valor':c8,
                     'variable':id_to_variable_nro['c8']
                    },
                   ]
                r = requests.post(url, headers=headers, json=data)
                print(r.text)       
                sleep(3)
