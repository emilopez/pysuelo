import os
import glob
import pytesseract
import pyocr
import pyocr.builders
import gpyocr
from PIL import Image


tools = pyocr.get_available_tools()
tool = tools[0]
dire = "/home/emiliano/data/dev/proyectos_git/pysuelo/src/py/data"
pics = glob.glob(f"{dire}/*.jpeg")
pics.sort()

display_box = 599, 651, 739, 703

print("filename, pyt, pyo, gpy")
for pic in pics[4:5]:
    img = Image.open(pic)
    img_display = img.crop(box=display_box)
    gray = img_display.convert('L')
    blackwhite = gray.point(lambda x: 0 if x < 150 else 255, '1')
    gray.save(f"{img.filename[-21:-5]}_gray.jpg")
    blackwhite.save(f"{img.filename[-21:-5]}_bw.jpg")
    box = [(3,  0, 32, 49),
     (32, 0, 59, 49),
     (59, 0, 83, 49),
     (83, 0, 109, 49),
     (109, 0, 135, 49),]

    #img_display.save(f"{img.filename[-21:-5]}.jpg")
    digits_pyt = pytesseract.image_to_string(blackwhite, lang= "fra", config="digits")
    digits_pyo = tool.image_to_string(blackwhite,
                                  lang="fra",
                                  builder=pyocr.tesseract.DigitBuilder())
    digits_gpy = gpyocr.tesseract_ocr(blackwhite,
                                      lang='fra',
                                      psm=12)[0]

    print(img.filename[-21:-5], digits_pyt, digits_pyo, digits_pyo, sep=",", end = "")
    print(",", end = "")
    digit_pyt = ""
    digit_gpy = ""
    for b in box:
        bwchar = blackwhite.crop(box=b)
        bwchar.show()
        digit_pyt += pytesseract.image_to_string(bwchar, lang= "fra", config="--psm 10 digits")
        digit_gpy += gpyocr.tesseract_ocr(bwchar, lang='fra', psm=10)[0]
    print(digit_pyt, digit_gpy, sep=",")

