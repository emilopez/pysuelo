#!/usr/bin/env python3

import serial
import datetime
from time import sleep
import sys
baud = 9600
baseports = ['/dev/ttyUSB', '/dev/ttyACM', 'COM', '/dev/tty.usbmodem1234']
ser = None

while not ser:
    for baseport in baseports:
        if ser:
            break
        for i in range(0, 64):
            try:
                port = baseport + str(i)
                ser = serial.Serial(port, baud, timeout=1)
                print("Monitor: Opened " + port + '\r')
                break
            except:
                ser = None
                pass

cant_sensores = 15
ronda_lectura = 10
periodo_minutos = 15
while True:
    for ronda in range(ronda_lectura):
        sensores_leidos = 0
        lecturas = list()
        while sensores_leidos < cant_sensores:
            val = str(ser.readline().decode().strip('\r\n'))#Capture serial output as a decoded string
            if val:
                sensores_leidos += 1
                val = val.replace("-",";-").replace("+",";")
                lecturas.append(val)
        timestamp = datetime.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
        fout = open("test_lectura_borrar.csv", "a")
        for lectura in lecturas:
            s = str(timestamp) + ";" + str(lectura)
            fout.write(s + "\n")
            print(s)
        print()
        fout.close()
        #sleep(1)
    print("ZZzzZZzz...")
    sleep(periodo_minutos*60)
