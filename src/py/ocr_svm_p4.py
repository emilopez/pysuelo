import plotly.express as px
from sklearn import datasets, svm

# recortar los dígitos de un display que tengan ¿iguales dimensiones? por?
# 

digits = datasets.load_digits()
model_svm = svm.SVC(gamma=0.001, C=100)
x,y = digits.data[:-10], digits.target[:-10]
model_svm.fit(x, y)

idx_img = -5
print('Prediction', model_svm.predict([digits.data[idx_img]]))
fig = px.imshow(digits.images[idx_img])
fig.show()